#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use File::Basename;
use Getopt::Long;
use Pod::Usage;
#use Data::Dump qw{dump};

use strict;
my ($help,$man,$opt_prefix);

our ($seqhome,$root,$flexible);
$root=(fileparse($0))[1];
$root=$ENV{MURASAKI} if $ENV{MURASAKI};
require "${root}/config.pl";
require "$root/common.pl";

my ($opt_x,$opt_y)=("tfidf","hits");
my $samples;
my $fn;
my $format="png";
my $lwd=3;
my ($opt_log,$opt_clean,$opt_nofstats,$fstats,%avg,$drawAvg,$maxsamples,$nofn);

GetOptions('help|?' => \$help, man => \$man, "x=s"=>\$opt_x,
	   'y=s' =>\$opt_y,
	   'log=s'=>\$opt_log, clean=>\$opt_clean, 'format=s'=>\$format,
	   pdf=>sub{$format='pdf'},'lwd=f'=>\$lwd,
	   fstats=>\$fstats, 'avg:s%'=>sub{
	     $drawAvg=1;
	     $avg{$_[1]}=$_[2] if $#_>1},
	   'noavg'=>sub{$drawAvg=0},nofstats=>\$opt_nofstats
	  ) or pod2usage(1);
pod2usage(1) if $help or $#ARGV<0;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

foreach my $file (@ARGV){
  my ($basename,$path,$suffix)=fileparse($file,qr/\.rocr?/);
  $path=~s!/$!!; #kill trailing / if any
  my $basefile="$path/$basename";
  my $rocrfile="$basefile.rocr";

  #attempt to grab a FN count from a filterstats file if we don't have one
  if(!$opt_nofstats){
    unless($fstats and -f $fstats){
      my @fstats=<$basefile.*.filterstat*>;
      $fstats=$#fstats>0 ? pickOne("Multiple filterstats files found:","Which should I use? ",@fstats):$fstats[0];
    }
    goto STARTSAMPLE unless -f $fstats;
    open(my $fstat_fh,$fstats);
    print "Reading stats from $fstats\n";
    while(<$fstat_fh>){
      if(m/Initial stats:/){
	<$fstat_fh>; #junk line about # of anchors
      GETMEANS: while(<$fstat_fh>){
	  $avg{$1}=$2 if m/^(\w+) mean: (\d+\.?\d*)/;
	  last GETMEANS unless m/^\w+ mean:/;
	}
      }
    }
    print "Found averages for: ".join(", ",sort keys %avg)."\n";
  }

 STARTSAMPLE:

  my (@fields);
  open(my $fh,$rocrfile);
  $_=<$fh>;
  chomp;
  @fields=split(/\t/,$_);
  print "Fields available: ".join(", ",@fields)."\n";

  my (@x,@y);
  @x=split(/\W/,$opt_x);
  @y=split(/\W/,$opt_y);
  if ($opt_x eq 'all') {
    @x=grep {$_ ne 'label'} @fields;
  }
  if ($opt_y eq 'all') {
    @y=grep {$_ ne 'label'} @fields;
  }

  print "Making graphs for $opt_x vs $opt_y\n";
  my %done;
  foreach my $x (@x) {
    foreach my $y (@y) {
      next if(exists($done{"$x-$y"}) or $x eq $y);
      $done{"$x-$y"}=1;

      my $tpfile="$basefile.stats.tp";
      my $fpfile="$basefile.stats.fp";
      my $rout="$basefile.$x.$y.$format";
      my $rsrc="$basefile.$x.$y.R";
      
      unless(-f $tpfile and -f $fpfile and !$opt_clean){ #reuse rocfile if there is one
	print "Splitting $rocrfile into TP and FP...\n";
	
	die "No rocr file: $rocrfile? Run filter.pl --rocr to make one..." unless -f $rocrfile;
	
	open(my $tpfh,">$tpfile") or die "Couldn't write to $tpfile\n";
	open(my $fpfh,">$fpfile") or die "Couldn't write to $fpfile\n";

	seek($fh,0,0);
	$_=<$fh>; #they each need a copy of the header
	print $tpfh $_;
	print $fpfh $_;

	while (<$fh>) {
	  my $target=(m/^1/ ? $tpfh : $fpfh);
	  print $target $_;
	}
	close($tpfh);
	close($fpfh);
      }
      
      my $legendpos="min(tp[,'$x'],fp[,'$x']),max(tp[,'$y'],fp[,'$y'])";
      my $outputter=$format ne 'pdf' ? 
	qq!bitmap(file="$rout",type="png16m",width=10,height=7,res=96)!:
	  qq!pdf(file="$rout",width=10,height=7)!;
      
      #do the R output
      my $tppch='1';
      my $fppch='16';
      open(my $R,">$rsrc");
      print $R <<ENDTEXT;
$outputter
tp<-read.delim('$tpfile')
fp<-read.delim('$fpfile')

xl<-c(min(tp[,'$x'],fp[,'$x']),max(tp[,'$x'],fp[,'$x']));
yl<-c(min(tp[,'$y'],fp[,'$y']),max(tp[,'$y'],fp[,'$y']));

plot(fp[,'$x'],fp[,'$y'],type='p',xlim=xl,ylim=yl,col=2,lwd=$lwd,xlab='$x',ylab='$y',main='$basename $x vs $y (TP/FP comparison)',log='$opt_log',pch=$fppch)
points(tp[,'$x'],tp[,'$y'],col=4,lwd=$lwd,pch=$tppch)
ENDTEXT
      
      if ($drawAvg and exists($avg{$x}) and  exists($avg{$y})) {
	print $R "abline(v=$avg{$x},lty=2,lwd=$lwd/2);\n";
	print $R "abline(h=$avg{$y},lty=2,lwd=$lwd/2);\n";
	print $R <<ENDTEXT;
text($avg{$x}*1.01,$avg{$y}*1.01,adj=c(0,1),labels="mean");
ENDTEXT
      }
      
      print $R <<ENDTEXT;
legend($legendpos,c("True Positives","False Positives"),col=c(4,2),pch=c($tppch,$fppch))
ENDTEXT
      close($R);
      system("R --vanilla < $rsrc");
    }
  }
}


sub sum {
  my $sum=0;
  grep {$sum+=$_} @_;
  return $sum;
}

sub mean {
  my $total;
  foreach(@_){
    $total+=$_;
  }
  return $total/($#_+1);
}

sub min {
  my $best=$_[0];
  foreach(@_){
    $best=$_ if $_<$best;
  }
  return $best;
}

sub max {
  my $best=$_[0];
  foreach(@_){
    $best=$_ if $_>$best;
  }
  return $best;
}

sub pickOne {
  my ($ps1,$ps2,@opts)=@_;
  print $ps1."\n";
  print map {($_==0 ? "[$_]":" $_ ").": $opts[$_]\n"} 0..$#opts;
  my $res;
  do{
    print $ps2;
    $res=<STDIN>;
    chomp $res;
  }while($res && ($res<0 or $res>$#opts));
  return $opts[$res];
}

__END__

=head1 NAME

stat-vs-stat.pl -- draws graphs of 1 stat vs another stat

=head1 SYNOPSIS

cbtest.pl <input1> [input2 ...]

=head1 OPTIONS

Input file should be some alignment that has as a .rocr file
(presumably generated by filter.pl --rocr). Output is graphed
to <basename>.cutoff.roc.png.

sensitivity/specificity requires a false negative count,
and as such requires a .filterstats file.

 Other options:
--stat|predictor|x specifies what stat to use as a predictor
--samples|n sets number of samples (default is sample at each possible cutoff)
--maxsamples|maxn sets a max for n/samples
--clean forces a re-sampling of the .rocr file
--log can apply log scale to x or y or xy axes
--fn can specify a FN count for calculating sensitivity
--avg preload averages (normally found from filterstats)
  (note: averages no drawn by default, so specify --avg (with no args) to
   enable them)
--noavg don't plot the line for averages
--lwd can specify line weight
--format can specify output file format (default png)
--pdf set format to pdf
--nofstats disable searching for filterstats statistics
--nofn pretend not to have fn statistics (ie: skip sensitivity)
--fstats specify the filterstats file to use
