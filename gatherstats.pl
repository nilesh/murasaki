#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use strict;

use Getopt::Long;
use Pod::Usage;
use File::Basename;
#use Data::Dump qw {dump};

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;
use Murasaki::KOG;
use Murasaki::Ticker qw{resetTick tick};

my $suffixre=qr/\.(anchors|anchors\.details|options|seqs|stdout|stderr|filterstats?|anchors\.filterstats?|anchors\.details\.filterstats?|murasaki)/;
our $podusageOpts={-message=>'Use --help or --man for more detailed help.',-verbose => 0,-exitval=>2};
my ($help,$man,$output,$disp_all,$opt_loadSeqs);
our ($completed_checked);
GetOptions('help|?' => \$help, man => \$man,
	   'output=s' => \$output, all => \$disp_all, loadseqs=>\$opt_loadSeqs
	    )
  or pod2usage({-verbose=>1,-exitval=>2});

pod2usage({-verbose=>1,-exitval=>2,-message=>'Need some input file...'}) if $#ARGV<0;

our %stats;
foreach my $arg (@ARGV){
  procArg($arg);
}

$disp_all=1 unless $completed_checked; #unless there's a possibility for confirming completeness, don't check it

my %fields=(file=>sub{my ($a)=@_;$a->{name}},
	    anchors=>sub{my ($a)=@_;$a->{anchors}},
	    time=>sub{my ($a)=@_;$a->{time}},
	    weight=>sub{my ($a)=@_;$a->{weight}},
	    length=>sub{my ($a)=@_;$a->{length}},
	    members=>sub{my ($a)=@_;join(",",$a->{cogspec})},
	    size=>sub{my ($a)=@_;$a->{alt_inputsize} ? $a->{alt_inputsize}:$a->{inputsize}},
	    seqs=>sub{my ($a)=@_;$a->{seqcount}},
	    Ksens=>sub{my ($a)=@_;$a->{rocp}->{KOGs}->{Sensitivity}},
	    Kspec=>sub{my ($a)=@_;$a->{rocp}->{KOGs}->{Specificity}},
	    sens=>sub{my ($a)=@_;$a->{rocp}->{KOGs}->{Sensitivity}},
	    spec=>sub{my ($a)=@_;$a->{rocp}->{KOGs}->{Specificity}},
	    Osens=>sub{my ($a)=@_;$a->{rocp}->{Orthos}->{Sensitivity}},
	    Ospec=>sub{my ($a)=@_;$a->{rocp}->{Orthos}->{Specificity}},
	    'auc-tfidf'=>sub{my ($a)=@_;$a->{predictor}->{auc}->{tfidf}},
	    'auc-hits'=>sub{my ($a)=@_;$a->{predictor}->{auc}->{hits}},
	    'auc-length'=>sub{my ($a)=@_;$a->{predictor}->{auc}->{length}}
	   );

my @fieldorder=qw{file weight length anchors time size seqs sens spec Osens Ospec auc-tfidf auc-hits auc-length};

$output="-" unless $output;
open(my $outfh,">$output");

print $outfh join("\t",@fieldorder)."\n";
print $outfh (map {my $s=$stats{$_};
	   join("\t",
		(map {
		  die "Unknown field $_" unless exists $fields{$_};
		  my $res=&{$fields{$_}}($s);
		  $res} @fieldorder))."\n"
		} (grep {$disp_all or exists $stats{$_}->{completed}} sort numberedFilesCmp keys %stats));

sub numberedFilesCmp {
  my $an=$1 if $a=~m/(\d+)/;
  my $bn=$1 if  $b=~m/(\d+)/;
  return $an <=> $bn if(defined($an) and defined($bn));
  return $a cmp $b;
}

sub procArg {
  my ($arg)=@_;
  if(-d $arg){
    opendir(my $dh,$arg);
    my @files=grep {$_ ne '.' and $_ ne '..'} readdir($dh);
    print "Scanning ".(@files)." files from $arg\n";
    resetTick(scalar(@files));
    foreach my $f (@files){
      procArg("$arg/$f");
      tick;
    }
    print "\n";
  }elsif(-f $arg){
    my ($name,$path,$suffix)=fileparse($arg,$suffixre);
    next unless $suffix; #only process known file types
#    print "Proc $arg as $name ~~ $suffix\n";
    $stats{$name}={name=>$name} unless ref $stats{$name} eq "HASH";
    my $statr=$stats{$name};
    open(my $fh,$arg);
    if($suffix eq ".seqs"){
      my $inputsize;
      while(<$fh>){
	chomp;
	push(@{$statr->{cogspec}},Murasaki::KOG->guessKogMember($_));
	$statr->{inputsize}+=getInputSize($_) if $opt_loadSeqs;
      }
      $statr->{seqcount}=lineCount($fh);
    }elsif($suffix=~m/^\.anchors(.details)?$/){
      $statr->{anchors}=lineCount($fh);
      $statr->{format}=anchorFormat($fh) if $suffix=~m/\.details$/;
    }elsif($suffix eq '.options'){
      while(<$fh>){
	chomp;
	next unless $_;
	if(m/(.*)?: (.*)/){
	  $statr->{options}->{$1}=$2;
	}else{
	  $statr->{options}->{$_}=undef;
	}
	$statr->{repeatmasked}=1 if $_ eq 'Repeats are: masked';
      }
    }elsif($suffix eq '.stdout' or $suffix eq '.murasaki'){
      $completed_checked=1;
      while(<$fh>){
	chomp;
	if(m/^Total anchors: /){
	  $statr->{completed}=1;
	}elsif(m/^Total processing time: (.*)/){
	  $statr->{time}=parseHumanTime($1);
	}elsif(m/^Pat: \((\d+) bases long\) contains (\d+) bits/){
	  $statr->{length}=$1;
	  $statr->{weight}=$2;
	}elsif(m/^Buckets used: (.*)/){
	  $statr->{bucketdat}=$1
	}elsif(m/^Output writing finished in: (.*)/){
	  $statr->{outputwrite_time}=parseHumanTime($1);
	}elsif(m/^Done \((\d+)bp\)/){
	  $statr->{alt_inputsize}+=$1;
	}
      }
    }elsif($suffix=~m/filterstats?$/){
      while(<$fh>){
      FILTERSTATCHECK:
	if(m/^ROCR derived predictor stats:/){ #grab AUC style stuff
	  $_=<$fh>;
	  s/^\s+//;
	  my @types=split(/\s+/);
	  do{
	    $_=<$fh>;
	    m/^\s*(\S+)\s+([0-9\. ]*)$/ or goto FILTERSTATCHECK;
	    my @bits=split(/\s+/,$2);
	    foreach my $i (0..$#types){
	      $statr->{predictor}->{$types[$i]}->{$1}=$bits[$i];
	    }
	  }while(1);
	}
	if(m/^Experimental (\w+):/){ #gather ROC
	  my $type=$1;

	  #get raw TP/FP/etc counts
	  $_=<$fh>;
	  while(m/(\w+: \d+)/g){
	    my ($stat,$score)=split(/: /,$1);
	    $statr->{roc}->{$type}->{$stat}+=$score;
	  }

	  #get summarized percent form
	  do {
	    $_=<$fh>;
	    chomp;
	    if(m/^(Sensiti.*|Specific.*|Preci.*: .*$)/i){
	      my ($stat,$score)=split(/: /,$1);
	      $statr->{rocp}->{$type}->{$stat}=$score;
	    }else{
	      goto FILTERSTATCHECK;
	    }
	  }while(1);
	}
      }
    }
  }elsif(!-e $arg){
    warn "Non-existant file input: $arg";
  }else{
    die "Don't know how to handle input: $arg";
  }
}

{
  my %sizeCache;
  sub getInputSize {
    my $fn=pop;
    return $sizeCache{$fn} if exists $sizeCache{$fn};
    return unless -f $fn;
    $sizeCache{$fn}=`$root/geneparse.pl -lfc $fn`;
    return $sizeCache{$fn};
  }
}

sub lineCount {
  my $count=0;
  my $fh=pop;
  seek($fh,0,0);
  while(<$fh>){
    $count++;
  }
  seek($fh,0,0);
  return $count;
}

sub anchorFormat {
  my $fh=pop;
  my $format=0;
  seek($fh,0,0);
  $_=<$fh>;
  $format=1 if m/^(?:-?\d+\s-?\d+\s[+-]\s*)+\d+$/;
  $format=2 if m/^(?:-?\d+\s-?\d+\s[+-]\s*)+\d+\s(?:\S+)\s(?:\d+:\d+,?){1,50}$/; #note: for some inputs, the member lists these lines can be craaaazy long.
  seek($fh,0,0);
  return $format;
}

__END__

=head1 NAME

gatherstats.pl -- gathers stats from all sorts of sources

=head1 SYNOPSIS

gatherstats.pl <input1> [input2 ...]

=head1 OPTIONS

Input is any bunch of files, or directories.
They'll be scanned for files from the various Murasaki programs,
like filterstats and stdout, etc.

 Options:
--loadseqs allows length checking from .seqs files
--output outputs to a file instead of STDOUT
