NAME
    mbfa - Murasaki Binary FASTA format converter

SYNOPSIS
     mbfa [options...] [input] [input2 ...] #convert [input] and [input2 ...] to Muraaski Binary FASTA files

DESCPRIPTION
    Murasaki processes sequence data using a 2-bit format where each base is
    reprsented using 2 bits. There's a number of pre-existing formats that
    do similar things, however in particular Murasaki needs to know about
    the metadata that can't be expressed in just 2 bits (eg. where sequences
    of NNNNs are, sequence breaks when multiple sequences are included in a
    FASTA file, etc.), therefore the MBFA format includes this data as well.
    Ordinarily these files are generated automatically by Murasaki when
    first run on a new sequence.

    Because the file format is designed mesh closely with Murasaki, the
    actual file extension will vary to reflect your architecture. It will
    generally be some form of .mbfa[48][48] (e.g. ".mbfa88" (the default gcc
    build on an amd64)).

OPTIONS
    --info|-i
        Show metadata about each MBFA specified.

    --force|-f
        By default mbfa will skip files that already have recent .mbfa
        files. This option forces the regeneration of these files.

    --fatal|-F
        Makes errors fatal. Ordinarily if you specify multiple files, mbfa
        will try to convert all of them even if one fails emitting a
        warning. With --fatal it will stop and exit with an error if there's
        a problem.

    --fasta|-A
        Geneates FASTA output corresponding based on the MBFA data to
        stdout.

    --help|-h, --version|-V, --verbose|-V
        What you'd expect.

LICENSE
    GNU General Public License, version 3 (GPLv3)

AVAILABILITY
    <http://murasaki.sourceforge.net>

AUTHOR
    Kris Popendorf <krisp@dna.bio.keio.ac.jp>

SEE ALSO
    murasaki(1), geneparse(1)

