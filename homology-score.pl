#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Long;
use Pod::Usage;
use File::Basename;
use IO::Handle;
#use Data::Dump qw {dump};

#use Math::BigRat;

use strict;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

my %aligners=(clustalw=>'clustalw -outfile=/dev/null',
	      muscle=>'muscle -out /dev/null');
my $aligner='muscle';
my $widen=0;
our $maxChunkSize=64000;
my $tmp='/tmp';
my ($help,$man);

GetOptions('help|?' => \$help, man => \$man,
	  'aligner=s' => \$aligner, 'widen=i'=>\$widen);
pod2usage(1) if $help or $#ARGV<0 or !$aligners{$aligner};
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

my ($inf,$outf)=@ARGV;

our $screenwidth=$ENV{COLUMNS};
$screenwidth=75 unless $screenwidth>0;

our ($infh,$outfh,@seqlist,%seqdata);
our $basepath=getPath($inf);
our $basename=(fileparse($inf,qr{\.[^\.]+}))[0];
our $basefile=getPath($inf).$basename;

$outf="$basefile.homology" unless $outf;

open($infh->{anchors},"$inf") or die "Can't open $inf for input";
open($infh->{seqs},"$basefile.seqs") or die "Couldn't load sequence file list";
open($outfh,">$outf");
our $anchorCount=lineCount($infh->{anchors});

@seqlist=readline($infh->{seqs});
chomp @seqlist;

$maxChunkSize/=@seqlist; #if there's lots of seqs, have to shorten chunks

foreach my $seq (@seqlist){
  print "Loading $seq...\n";
  $seqdata{$seq}=`$root/geneparse.pl -c $seq`;
}

my $anchor=0;

print "Aligning $anchorCount anchors\n";
resetTick();
while(my $line=readline($infh->{anchors})){
  my $i=0;
  my (%res,@chunks);
  #make all the chunks
  while($line=~m/(-?\d+)\s(-?\d+)\s([+-])\s*/g){
    my ($start,$stop,$strand)=($1-$widen,$2+$widen,$3);
    ($start,$stop)=(abs($stop),abs($start)) if $strand eq '-';
    my $dna=substr($seqdata{$seqlist[$i]},$start-1,$stop-$start+1);
    if($strand eq '-'){
      $dna=~tr/ACGT/TGCA/;
      $dna=reverse $dna;
    }
    my ($chunkid,$chunkstart,$chunksize)=(0,0,min(length($dna),$maxChunkSize));
    while($chunkstart<length($dna)){
      my $chunk=substr($dna,$chunkstart,$chunksize);
      push(@{$chunks[$i]},$chunk);
#      print "Anchor $anchor . $chunkid size: ".length($chunk)."\n";
    }continue{$chunkstart+=$maxChunkSize,$chunkid++}
  }continue{$i++};

  $i=0;
  do{
    my @bits=map {shift(@$_)} @chunks;
    my %chunkres=alignChunk("$anchor.$i",\@bits);
    foreach my $k (keys(%chunkres)){
      $res{$k}+=$chunkres{$k};
    }
  }while(grep {scalar(@$_)} @chunks);

  my $score=join("\t",@res{0..$#seqlist});

  print $outfh $score."\n";
  tick();
}continue{$anchor++};
print "\n";
print "Done!\n";

sub alignChunk {
  my ($chunkname,$chunks)=@_;
  my $tmpbase="$tmp/$basename.$chunkname";
  my $tmpfile="$tmpbase.mfa";
  open(my $ofh,'>',$tmpfile);
  my $i=0;
  foreach my $dna (@$chunks){
    print $ofh ">$i\n",$dna,"\n";
  }
  close $ofh;

  my @treeDat;
  if($aligner eq 'clustalw'){
    my $res=`$aligners{$aligner} -infile=$tmpfile -tree`;
    $res=~m/Phylogenetic tree file created:\s+\[(.+)\]/m or die "Uh oh, aligner puked:\n$res";
    my $rawtree=slurp($1);
    @treeDat=tokenizeTree($rawtree);
  }elsif($aligner eq 'muscle'){
    my $cmd="$aligners{$aligner} -in $tmpfile -quiet -tree1 $tmpfile.tree1 -tree2 $tmpfile.tree2 -maxiters 2";
    my $res=`$cmd`;
    my $useTree="$tmpfile.tree2";
    $useTree="$tmpfile.tree1" unless -f $useTree;
    open(my $resh,$useTree) or die "No treefile? (Chunk $chunkname)\n";
    @treeDat=<$resh>;
    chomp @treeDat;
    @treeDat=@treeDat[0..($#treeDat-1)]; #dont need last line
  }
  my %res;
  parseTree(\%res,0,@treeDat);
  system("rm $tmpbase*"); #cleanup
  return %res;
}

sub tokenizeTree {
  my $num='\d+.?\d*';
  my @tokens;
  foreach(split(/\s/,join("\t",@_))){
    while(m/\(|\)(:$num)?|,|\S+:$num|;/g){
      push(@tokens,$&)
    }
  }
  return @tokens;
}

sub sum {
  my $sum=0;
  grep {$sum+=$_} @_;
  return $sum;
}

sub min {
  my $best=$_[0];
  foreach(@_){
    $best=$_ if $_<$best;
  }
  return $best;
}

sub parseTree {
  my $resr=shift;
  my $root=shift;
  return unless @_;
  my ($first)=(shift);
  if($first eq ','){
    return parseTree($resr,$root,@_);
  }elsif($first=~m/^\(/){ #subtree
    my ($in,$last,$out)=findPair(@_);
    my $dist=$1 if $last=~m/\):(\S+)/;
    parseTree($resr,$root+$dist,@$in);
    return parseTree($resr,$root,@$out);
  }elsif($first=~m/(\S+):(\S+)/){ #leaf
    $resr->{$1}=$root+$2;
    return parseTree($resr,$root,@_);
  }
}

sub findPair {
  my (@in,$inside);
  while($_=shift){
    if($_ eq '('){
      $inside++;
    }
    elsif(m/^\)/){
      $inside--;
      return (\@in,$_,\@_) if $inside<0;
    }else{
    }
    push(@in,$_);
  }
  die "Uh oh. Tree is missing $inside )'s";
}

sub getName {
  my @ret=map {
    my ($name,$path,$suffix) = fileparse($_, qr{\.[^.]*});
    $name
    } @_;
  return @ret if $#_;
  return $ret[0];
}

sub getPath {
  my @ret=map {
    my ($name,$path,$suffix) = fileparse($_, qr{\.[^.]*});
    $path
    } @_;
  return @ret if $#_;
  return $ret[0];
}

sub anchorCountSeqs {
  my $fh=pop;
  my $count=0;
  seek($fh,0,0);
  $_=<$fh>;
  while(m/^(-?\d+\s-?\d+\s[+-]\s*)$/){
    $count++;
  }
  seek($fh,0,0);
  return $count;
}

sub anchorFormat {
  my $fh=pop;
  my $format=0;
  seek($fh,0,0);
  $_=<$fh>;
  $format=1 if $_=~m/^(-?\d+\s-?\d+\s[+-]\s*)+\d+$/;
  $format=2 if $_=~m/^(-?\d+\s-?\d+\s[+-]\s*)+\d+\s(\S+)\s(\d+:\d+,?)+$/;
  seek($fh,0,0);
  return $format;
}

sub lineCount {
  my $count=0;
  my $fh=pop;
  seek($fh,0,0);
  while(<$fh>){
    $count++;
  }
  seek($fh,0,0);
  return $count;
}

sub slurp {
  local $/;
  open(my $fh,"@_") or return;
  return <$fh>;
}

sub resetTick {
  my ($total,$div)=@_;
  our ($anchorCount,$screenwidth);
  ($total,$div)=($anchorCount,$screenwidth) unless $total;
  our ($ticksper,$ticksleft)=(int($total/$div),int($total/$div));
  print "|",(map {"-"} (3..$div)),"|\n";
}

sub tick {
  our ($ticksper,$ticksleft);
  $ticksleft--;
  if(!$ticksleft){
    print STDOUT ".";
    STDOUT->flush();
    $ticksleft=$ticksper;
  }
}

__END__

=head1 NAME

homology-score.pl - generates homology scorse from murasaki anchor files

=head1 SYNOPSIS

filter.pl [options] <murasaki ailgnment file> [output file]

=head1 OPTIONS

 --aligner=<alignment program> => Selects the aligner program.
     Options are clustalw (slow but won't die on large anchors),
     or muscle (fast, but dies on anchors longer than 20k).
     Default is Muscle.
