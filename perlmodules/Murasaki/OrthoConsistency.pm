#!/usr/bin/perl

#Murasaki - multiple genome global alignment program
#Copyright (C) 2006-2007 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;
package Murasaki::OrthoConsistency;

#use Data::Dump qw{dump};
use Murasaki::Ticker qw{resetTick tick};
#no Carp::Assert;

sub empty {
  my ($class,$name,$kogmap,$knownLocs)=@_;
  bless {orthos=>{},name=>$name,knownLocs=>$knownLocs,spec2pos=>reverseMap($kogmap)},$class;
}

sub clone {
  my ($class,$base,$newName)=@_;
  my %content=%{$base};
  $content{name}=$newName if $newName;
  bless {%content},$class;
}

sub canonicalize {
  my ($members)=@_;
  return join("~",map {keys(%$_)} (sort {(values(%$a))[0] cmp (values(%$b))[0]} @$members));
}

sub canonParts {
  my ($canon)=@_;
  return split(/~/,$canon);
}

sub addBits {
  my ($basis,$next,@others)=@_;
  if($next){
    my @bits;
    foreach my $bit (@$next){
      push(@bits,addBits([@$basis,$bit],@others));
    }
    return @bits;
  }else{
    return $basis;
  }
}

sub getLocPairs {
  my @bits=@_;
  my @res;
  foreach my $i (0..$#bits){
    foreach my $j (($i+1)..$#bits){
      push(@res,[@{$bits[$i]},@{$bits[$j]}]);
    }
  }
  return @res;
}

sub getPairs {
  my @bits=@_;
  my @res;
  foreach my $i (0..$#bits){
    foreach my $j (($i+1)..$#bits){
      push(@res,[$bits[$i],$bits[$j]]);
    }
  }
  return @res;
}

sub isKnownLoc {
  my ($self,$loc)=@_;
  die "Bad loc: ".dump($loc) unless ref $loc eq 'HASH' and scalar(keys %$loc)==1;
  my $l=(keys %$loc)[0];
  return exists $self->{knownLocs}->{$l} and $self->{knownLocs}->{$l} eq $loc->{$l};
}

sub addAll {
  my ($self,$memr)=@_;
  our @unfolded=addBits([],(ref $memr eq 'ARRAY') ? (@$memr):
			(values(%{reverseMap($memr)})));
  foreach my $subset (@unfolded){
    my $canon=canonicalize($subset);
    my $content={ortho=>1};
    if(!grep {!$self->isKnownLoc($_)} @$subset){
      $content->{known}=1;
      my ($p1,$p2)=map {canonicalize([$_])} @$subset;
      my $canon=canonicalize($subset);
      $self->{knownOrthos}->{$p1}->{$canon}=1;
      $self->{knownOrthoList}->{$canon}=1;
    }
    $self->{orthos}->{$canon}=$content;
  }
}

sub hitAllKnown {
  my ($self,@perSeqLocs)=@_;
  my @locs=map {@$_} @perSeqLocs; #flatten
  my %hits=map {canonicalize([$_])=>1} @locs; #hash;
  my %res;

  #for each loc that we hit, if it didn't meet its intended partners, we record a miss (otherwise it's a hit)
  my %counted;
  foreach my $loc (@locs){
    next unless $self->isKnownLoc($loc);
    my $p1=canonicalize([$loc]);
    foreach my $canon (keys %{$self->{knownOrthos}->{$p1}}){
      next if $counted{$canon};
      $counted{$canon}=1;
      my @parts=canonParts($canon);
      if(grep {!$hits{$_}} @parts){ #hit all constituent parts?
	$self->{orthos}->{$canon}->{missed}++;
	$res{misses}++;
      }else{
	$self->{orthos}->{$canon}->{hit}++;
	$self->{orthos}->{$canon}->{hitonce}=1;
	$res{hits}++;
      }
    }
  }
  return %res;
}

sub reverseMap {
  my $arg=pop;
  my $ret={};
  if(ref $arg eq 'ARRAY'){
    for(0..$#$arg){
      $ret->{$$arg[$_]}=$_;
    }
  }elsif(ref $arg eq 'HASH'){
    for my $k (keys(%$arg)){
      push(@{$ret->{$arg->{$k}}},{$k=>$arg->{$k}});
    }
  }else{
    print "Non-reversable: ".dump($arg)."....\n";
    die "Non-reversable arg called on reverseMap...\n";
    $ret=$arg; #ummmm
  }
  return $ret;
}

sub toSpecList {
  my ($self,$members)=@_;
  my @ret;
  foreach my $k (keys(%$members)){
    do{print "Unknown species $k.\n"; return undef} unless exists($self->{spec2pos}->{$k});
    $ret[$self->{spec2pos}->{$k}]=$members->{$k};
  }
  return @ret;
}

sub orthosFromKog {
  my ($class,$kogs,$kogmap,$knownLocs)=@_;
  my $this=empty($class,$kogs->{kogSrc},$kogmap,$knownLocs); #this isnt C++. we can get our object up and going now ^^
  print "Filling OrthoPairs from KOGset\n";
  foreach my $kogname (keys(%{$kogs->{kogs}})){
    $this->addAll($kogs->{kogs}->{$kogname}->{members})
  }
  print $kogs->{kogSrc}." converted to OrthoPairs list. ".$this->summary."\n";
  return $this;
}

sub summary {
  my ($self)=@_;
  my $count=scalar(keys %{$self->{orthos}});
  return $count.($count==1 ? " ortholog":" orthologs");
}

sub possibleOrthosCount {
  my ($self)=@_;
  my %count;
  foreach my $s (values(%{$self->{knownLocs}})){
    $count{$s}++;
  }
  return 0 unless keys(%count);
  my $possible=1;
  foreach my $s (keys(%count)){
    $possible*=$count{$s};
  }
  return $possible;
}

sub addOrtho {
  my ($self,@set)=@_;
  foreach my $subset (addBits([],@set)){
    $self->{orthos}->{canonicalize($subset)}=1;
  }
}

sub isOrtho {
  my ($self,@set)=@_;
  my %res;
  foreach my $subset (addBits([],@set)){
    $res{($self->{orthos}->{canonicalize($subset)}) ? "yes":"no"}++;
  }
  return %res;
}

sub stats { #both of those should just be one function
  my ($self)=@_;
  my %counted;
  my ($conCount,$inconCount,$recallCount)=(0,0,0);
  foreach my $hitloc (keys %{$self->{knownOrthos}}){
    foreach my $canon (keys %{$self->{knownOrthos}->{$hitloc}}){
      next if $counted{$canon}; #thou shalt not double count
      $conCount+=$self->{orthos}->{$canon}->{hit};
      $inconCount+=$self->{orthos}->{$canon}->{missed};
      $recallCount+=$self->{orthos}->{$canon}->{hitonce};
      $counted{$canon}=1;
    }
  }
  my $totalOrthos=keys %{$self->{knownOrthoList}};
  my %res=(recall=>$recallCount,
	   con=>$conCount,
	   incon=>$inconCount,
	   total=>$totalOrthos);
  $res{sens}=$res{recall}/$res{total} if $res{total};
  $res{spec}=$conCount/($conCount+$inconCount) if ($conCount+$inconCount);
  return %res;
}

sub compare {
  my ($self,$other)=@_;
  my %done;
  my %res=(in1=>0,in2=>0,inboth=>0);
  print "Comparing ortholist members...\n";
  @done{keys(%{$self->{orthos}})}=(1) x scalar keys(%{$self->{orthos}});
  my @inboth;
  resetTick(scalar(keys(%{$other->{orthos}})));
#  print "Done: ".join("\n",keys(%done))."\n";
  foreach my $ortho (keys(%{$other->{orthos}})){
#    print "Compare: $ortho\n";
    if($done{$ortho}){
      $res{inboth}++;
      push(@inboth,$ortho);
      delete $done{$ortho};
    }else{
      $res{in2}++;
    }
    tick;
  }
  $res{in1}+=scalar keys %done;
#  print "\nOrtho inboth:\n".join("\n+",@inboth)."\n";
#  print "\nOrtho in source:\n".join("\n*",keys %{$self->{orthos}})."\n";
  if($self->{knownLocs} == $other->{knownLocs}){#impossible to know otherwise
    $res{in0}=$self->possibleOrthosCount()-$res{in1}-$res{in2}-$res{inboth};
  }
  print "\n";
  return %res;
}

1;
