#!/usr/bin/perl

#Murasaki - multiple genome global alignment program
#Copyright (C) 2006-2007 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

package Murasaki::Ticker;
use IO::Handle;

use Exporter 'import';
@EXPORT_OK=qw{resetTick tick};

our $screenwidth=75;
$screenwidth=$ENV{COLUMNS} if(defined($ENV{COLUMNS}) and $ENV{COLUMNS}>0);

sub resetTick {
  my ($total,$div)=@_;
  our ($anchorCount,$screenwidth);
  $div=$screenwidth unless $div;
  $total=$anchorCount unless $total;
  our ($ticksper,$ticksleft)=(int($total/$div),int($total/$div));
  print "|",(map {"-"} (3..$div)),"|\n";
}

sub tick {
  our ($ticksper,$ticksleft);
  $ticksleft--;
  if(!$ticksleft){
    print STDOUT '.';
    STDOUT->flush();
    $ticksleft=$ticksper;
  }
}
1;
