#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

package Murasaki::SeqFeatures;

use Exporter 'import';
@EXPORT=qw{readCds writeCds};

use Bio::SeqFeature::Generic;

sub readCds {
  my $file=shift;
  open(my $infh,"<",$file);
  my $offset=shift;
  $offset=0 unless $offset;
  my @features;
  while(<$infh>){
    my @a=split(/\s/,$_);
    unless($#a>2){
      print "Couldn't parse line $file $.: $_\n";
      next;
    }
    push(@features,new Bio::SeqFeature::Generic
	 (-primary => CDS,
	  -tag => {($a[0] ? (gene => $a[0]):()), ($a[4] ? (locus_tag=>$a[4]):())},
	  -start => $offset+$a[1],
	  -end => $offset+$a[2],
	  -strand => $a[3])
	);
  }
  close($infh);
  return @features;
}

sub writeCds {
  my $outf=shift;
  my ($gene_src,$locus_src)=("gene","locus_tag");
  $locus_src=$common_overrides{locus} if $common_overrides{locus};
  $gene_src=$common_overrides{gene} if $common_overrides{gene};
  open(my $outfh,">",$outf) or die "Couldn't open $outf for writing";
  foreach(@_){
    my ($name,$locus);
    $name=join(";",$_->get_tag_values($gene_src)) if $_->has_tag($gene_src);
    $name=join(";",$_->get_tag_values('gene')) if $_->has_tag('gene') && !$name;
    $name=join(";",$_->get_tag_values('protein_id')) if $_->has_tag('protein_id') && !$name;
    $name=join(";",$_->get_tag_values('product')) if $_->has_tag('product') && !$name;
    $name=$common_overrides{setName}  if exists $common_overrides{setName};
    $locus=join(";",$_->get_tag_values($locus_src)) if $_->has_tag($locus_src);
    if($common_overrides{extract} and $locus){
      my $pat=$common_overrides{extract};
      $locus=~m/$pat/;
      $locus=$1 if $1;
    }
    if($common_overrides{prefix} and $locus){ #I'm looking at you EcZ....
      $locus=$common_overrides{prefix}.$locus;
    }
    if($common_overrides{autoenum}){ #Cgl tRNA. no locus names
      our $lastLocusId;
      $locus.=$common_overrides{autoenum}.++$lastLocusId;
    }
    ($name,$locus)=map { $_ ? $_:"" } ($name,$locus);
    foreach($name,$locus){ $_=~s/\s/_/g };
    print $outfh join("\t",$name,
		      $_->location->start,
		      $_->location->end,
		      $_->location->strand,
		      $locus),"\n";
  }
  close($outfh);
}

1;
