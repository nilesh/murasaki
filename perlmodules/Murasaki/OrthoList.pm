#!/usr/bin/perl

#Murasaki - multiple genome global alignment program
#Copyright (C) 2006-2007 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;
package Murasaki::OrthoList;

#use Data::Dump qw{dump};
use Murasaki::Ticker qw{resetTick tick};

sub empty {
  my ($class,$name,$kogmap,$knownLocs)=@_;
  bless {orthos=>{},name=>$name,knownLocs=>$knownLocs,spec2pos=>reverseMap($kogmap)},$class;
}

sub canonicalize {
  my ($members)=@_;
  return join("~",map {keys(%$_)} (sort {(values(%$a))[0] cmp (values(%$b))[0]} @$members));
}

sub addBits {
  my ($basis,$next,@others)=@_;
  if($next){
    my @bits;
    foreach my $bit (@$next){
      push(@bits,addBits([@$basis,$bit],@others));
    }
    return @bits;
  }else{
    return $basis;
  }
}

sub addAll {
  my ($self,$memr)=@_;
  our @unfolded=addBits([],(ref $memr eq 'ARRAY') ? (@$memr):
			(values(%{reverseMap($memr)})));
  foreach my $subset (@unfolded){
    $self->{orthos}->{canonicalize($subset)}=1;
  }
}

sub reverseMap {
  my $arg=pop;
  my $ret={};
  if(ref $arg eq 'ARRAY'){
    for(0..$#$arg){
      $ret->{$$arg[$_]}=$_;
    }
  }elsif(ref $arg eq 'HASH'){
    for my $k (keys(%$arg)){
      push(@{$ret->{$arg->{$k}}},{$k=>$arg->{$k}});
    }
  }else{
    print "Non-reversable: ".dump($arg)."....\n";
    die "Non-reversable arg called on reverseMap...\n";
    $ret=$arg; #ummmm
  }
  return $ret;
}

sub toSpecList {
  my ($self,$members)=@_;
  my @ret;
  foreach my $k (keys(%$members)){
    do{print "Unknown species $k.\n"; return undef} unless exists($self->{spec2pos}->{$k});
    $ret[$self->{spec2pos}->{$k}]=$members->{$k};
  }
  return @ret;
}

sub orthosFromKog {
  my ($class,$kogs,$kogmap,$knownLocs)=@_;
  my $this=empty($class,$kogs->{kogSrc},$kogmap,$knownLocs); #this isnt C++. we can get our object up and going now ^^
  print "Filling ortholist from KOGset\n";
  foreach my $kogname (keys(%{$kogs->{kogs}})){
    $this->addAll($kogs->{kogs}->{$kogname}->{members})
  }
  print $kogs->{kogSrc}." converted to ortholog list. ".$this->summary."\n";
  return $this;
}

sub summary {
  my ($self)=@_;
  my $count=scalar(keys %{$self->{orthos}});
  return $count.($count==1 ? " ortholog":" orthologs");
}

sub possibleOrthosCount {
  my ($self)=@_;
  my %count;
  foreach my $s (values(%{$self->{knownLocs}})){
    $count{$s}++;
  }
  return 0 unless keys(%count);
  my $possible=1;
  foreach my $s (keys(%count)){
    $possible*=$count{$s};
  }
  return $possible;
}

sub addOrtho {
  my ($self,@set)=@_;
  foreach my $subset (addBits([],@set)){
    $self->{orthos}->{canonicalize($subset)}=1;
  }
}

sub isOrtho {
  my ($self,@set)=@_;
  my %res;
  foreach my $subset (addBits([],@set)){
    $res{($self->{orthos}->{canonicalize($subset)}) ? "yes":"no"}++;
  }
  return %res;
}

sub compare {
  my ($self,$other)=@_;
  my %done;
  my %res=(in1=>0,in2=>0,inboth=>0);
  print "Comparing ortholist members...\n";
  @done{keys(%{$self->{orthos}})}=(1) x scalar keys(%{$self->{orthos}});
  my @inboth;
  resetTick(scalar(keys(%{$other->{orthos}})));
#  print "Done: ".join("\n",keys(%done))."\n";
  foreach my $ortho (keys(%{$other->{orthos}})){
#    print "Compare: $ortho\n";
    if($done{$ortho}){
      $res{inboth}++;
      push(@inboth,$ortho);
      delete $done{$ortho};
    }else{
      $res{in2}++;
    }
    tick;
  }
  $res{in1}+=scalar keys %done;
#  print "\nOrtho inboth:\n".join("\n+",@inboth)."\n";
#  print "\nOrtho in source:\n".join("\n*",keys %{$self->{orthos}})."\n";
  if($self->{knownLocs} == $other->{knownLocs}){#impossible to know otherwise
    $res{in0}=$self->possibleOrthosCount()-$res{in1}-$res{in2}-$res{inboth};
  }
  print "\n";
  return %res;
}

1;
