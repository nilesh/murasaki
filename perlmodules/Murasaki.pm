#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

package Murasaki;

use File::Basename;
use strict;

use base 'Exporter';

our ($root,@EXPORT,@EXPORT_OK);

BEGIN {
  our $root;
  unless($root){
    my $runby=$0;
    $runby=`which $runby` unless $runby=~m!/!; #unless absolute path, grab real path this way. unpleasant hack. open to suggestions.
    $root=(fileparse($runby))[1];
    $root=$ENV{MURASAKI} if $ENV{MURASAKI};
  }
  @EXPORT=qw{$root readableTime parseHumanTime getProg};
  @EXPORT_OK=qw{$root repSuffix writeOut min max median readfile};
}


sub repSuffix {
  my ($name,$path,$suffix) = fileparse($_[0], qr{\.[^.]*});
  return $path.$name.$_[1];
}

sub writeOut {
  my $filename=shift;
  open(my $ofh,">$filename") or die "Ack! Couldn't open $filename for writing!\n";
  print $ofh @_;
}

sub readableTime {
  my $t=shift;
  my $str="";
  my %r = (
  d => int($t/(60*60*24)),
  h => int($t%(60*60*24)/60/60),
  m => int($t%(60*60)/60),
  s => $t % 60);
  join(" ",map { "$r{$_}$_" } grep { $r{$_} } qw{d h m s});
}

sub parseHumanTime {
  my $total;
  my %r=( d => 24*60*60,
	  day => 24*60*60,
	  days => 24*60*60,
	  hour => 60*60,
	  hours => 60*60,
	  h => 60*60,
	  'minute' => 60,
	  'minutes' => 60,
	  'm' => 60,
	  'second' => 1,
	  'seconds' => 1,
	  's' => 1,
      );
  my $line=pop;
  my $tre=join("|",keys(%r));
  if($line=~m/\d+($tre)/){
    foreach (split(/\s+/,$line)){
      m/(\d+\.?\d*)(\w)/ or warn "Not a time bit: $_";
      warn "Unknown time unit: $2" unless exists($r{$2});
      $total+=$1*$r{$2};
    }
  }else{
    while($line=~m/(\d+\.?\d*) ($tre)/g){
      $total+=$1*$r{$2};
    }
  }
  return $total;
}

sub getProg {
  my (@l)=@_;
  our $root;
  my (@r)=map {my $name=$_;
	       my $p="$root/$name";
	       $p="$root/$name.pl" unless -x $p;
	       -x $p ? $p:undef
	     } @l;
  return @r if wantarray;
  return $r[0];
}

sub readfile {
  my ($file)=@_;
  open(my $fh,$file);
  local $/;
  my $dat=<$fh>;
  if(wantarray){
    return split(/\n/,$dat);
  }else{
    return $dat;
  }
}

sub max {
  my ($m,@l)=@_;
  foreach my $x (@l){
    $m=$x if $x>$m;
  }
  return $m;
}

sub min {
  my ($m,@l)=@_;
  foreach my $x (@l){
    $m=$x if $x<$m;
  }
  return $m;
}

sub median {
  my (@l)=sort {$a<=>$b} @_;
  return $l[int($#l/2)]if(@l%2==1);
  return ($l[int($#l/2)]+$l[int($#l/2)+1])/2;
}

1;
