#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Std;
use List::Util shuffle;

$Getopt::Std::STANDARD_HELP_VERSION=true;
getopts('chs:t:n');
if($opt_h){HELP_MESSAGE();exit(0);}

srand($opt_s) if defined($opt_s);

my $repeats=$opt_t ? $opt_t:1;

($weight,$length,$file)=@ARGV;
($weight,$length,$file)=($1,$2,$length) if $weight=~m/(\d+)\D(\d+)/;
$zeros=$length-$weight;

if($file){
  open(OF,">>$file");
  select OF;
}

print "Weight: $weight\nLength: $length\n" unless $opt_c;

for my $i (1..$repeats){
  $pat=join("",1,
	    shuffle((map {1} (1..$weight-2)),(map {0} (1..$zeros))),
	    1);

  print $pat;
}continue{print "\n" if $i < $repeats}

print "\n" unless $opt_n;

sub main::HELP_MESSAGE(){
  print <<ENDTEXT
Usage: $0 [-c] <weight> <length>
  -c specifies "clean output" ie only the pattern
  -n suppress the final newline
  -s specifies seed for random number generator
  -t specifies the number of patterns to generate

As an alternative to specifying <weight> <length> and you can also use
the Murasaki format of <weight>:<length>.

A neat trick for scripts is to use geneparse.pl to generate this for you.
 eg: ./murasaki -p`./geneparse.pl -m -f -e -c seq/humanY.fa`
ENDTEXT
    ;
}
  
  sub main::VERSION_MESSAGE(){
  }
