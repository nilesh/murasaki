#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Std;
use List::Util shuffle;
use File::Basename;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

$Getopt::Std::STANDARD_HELP_VERSION=true;
getopts('chn');
if($opt_h){HELP_MESSAGE();exit(0);}

($filename,$outfile)=@ARGV;

if($filename and -e $filename){
  $inseq=`$root/geneparse.pl -c $filename`;
  my ($basename,$path,$suffix) = fileparse($filename);
  $name="$basename-shuffled";
} else {
  print STDERR "File $filename not found. Waiting for input from stdin.\n" unless !$filename or $filename eq "-";
  while($_=<STDIN>){
    chomp;
    $inseq.=$_;
  }
  $name="stdin";
}
$outfile="-" unless $outfile;
if($opt_c){
  open(OUTF,">$outfile");
}else{
  open(OUTF,"|$root/faformat.pl --name=\"$name\" - $outfile");
}

$inseq=~s/[^atgc]//gi unless $opt_n;
print OUTF join("",shuffle(split(//,$inseq)));


sub main::HELP_MESSAGE(){
  print <<ENDTEXT
Usage: $0 [-c] [-n] [<in file>] [<out file>]

If you don't specify an infile or outfile, stdin/stdout is used.
 -c specifies clean output (ie: just the sequence)
 -n specifies to preserve non-atgc data.
     (Default behaviour is to eliminate them).
ENDTEXT
    ;
}

sub main::VERSION_MESSAGE(){
}
