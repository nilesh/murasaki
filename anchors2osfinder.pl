#!/usr/bin/perl -w

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use strict;

use Getopt::Long;
use Pod::Usage;
use File::Basename;
#use Data::Dump qw {dump};

use strict;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}

use Murasaki;
use Murasaki::Ticker qw{resetTick tick};
our $root;

my ($man,$help,$makecds);
my (@seqnames,$output_opt);
my $opterr=
  GetOptions('help|?' => \$help, man => \$man,
	    'output=s'=>\$output_opt);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

pod2usage({-verbose=>1,-exitval=>2,-message=>'Need some input file...'}) if $#ARGV<0;

my $round;
foreach my $inf (@ARGV){
  my ($iprefix,$dir,$ext)=fileparse($inf,qr{\.anchors});
  die "Need a .anchors file." unless $ext eq ".anchors";
  my $seqfile="$dir/$iprefix.seqs";
  die "Need $seqfile" unless -f $seqfile;
  open(my $infh,$inf) or die "Couldn't open $inf";
  my @seqs=map { {file=>$_} } (split(/\n/,slurp($seqfile)));
  foreach my $seq (@seqs){
    print "Loading regions for $seq->{file}\n";
    $seq->{regions}=[getRegions($seq->{file})];
  }

  my $outf=($output_opt and $round) ? "$output_opt.$round":$output_opt;
  $outf="$inf.osfinder-in" unless $outf;
  print STDERR "Warning: Overwriting $outf\n" if -f $outf;
  my ($lines)=`wc -l $inf`=~m/(\d+)/ if -f $inf;
  resetTick($lines) if $lines;
  open(my $ofh,">$outf") or die "Can't write $outf";
  while(my $line=<$infh>){
    my @anchors=getAnchors($line);
    die "Illegal number of anchors (".scalar(@anchors).")" unless scalar(@anchors)==scalar(@seqs);
    print $ofh join("\t",
		    map {fixInRegion($anchors[$_],$seqs[$_]->{regions})} 0..$#anchors)."\n";
    tick() if $lines;
  }
  print STDERR "\nDone with $inf\n";
}

sub mean {
  my $s;
  foreach my $e (@_){
    $s+=$e;
  }
  return $s/scalar(@_);
}

sub cmpRegion {
  my ($needle,$region)=@_;
  return 0 if ($needle->{stop}<=$region->{stop} and $needle->{start}>=$region->{start});
  return $needle->{start}<=>$region->{start}; #this only works non-overlapping regions, which these regions are
}

sub shortAnchor {
  my ($anchor)=@_;
  return $anchor->{start}."~".$anchor->{stop};
}

sub binaryRegionSearch {
  my ($needle,$haystack)=@_;
  my ($min,$max)=(0,$#{$haystack});
#  print "Looking for ".shortAnchor($needle)." out of $max regions\n";
  do {
    my $p=int(mean($min,$max));
#    print "min: $min (".shortAnchor($$haystack[$min]).") p: $p(".shortAnchor($$haystack[$p]).") max: $max(".shortAnchor($$haystack[$max]).")";
    my $cmp=cmpRegion($needle,$$haystack[$p]);
#    print " cmp: $cmp\n";
    if($cmp==0){
      return ($p,$$haystack[$p]);
    }elsif($cmp<0){ #in left half
      $max=$p-1;
    }else{
      $min=$p+1;
    }
  }while($min<=$max);
  return undef;
}

sub fixInRegion {
  my ($anchor,$regions)=@_;
  return (1,$anchor->{start},$anchor->{stop}) unless $regions;
  die "programming error ftl!" unless ref($regions) eq 'ARRAY';
  my ($i,$region)=binaryRegionSearch($anchor,$regions);
  die "Uh oh. No matching region found for anchor: ".anchorToString($anchor) unless $region;
  if($anchor->{start}>=$region->{start} and $anchor->{stop}<=$region->{stop}){
    return ($i+1,$anchor->{start}-$region->{start}+1,$anchor->{stop}-$region->{start}+1,$anchor->{sign});
  }else{
    die "Uh oh. my binary search is very broken";
  }
}

sub anchorToString {
  my ($anchor)=@_;
  return join("\t",$anchor->{start},$anchor->{stop},$anchor->{sign});
}

sub getAnchors {
  my ($line)=@_;
  chomp $line;
  my @d=split(/\t/,$line);
  my @anchors;
  while(@d){
    my ($start,$stop,$sign)=map {shift(@d)} (1..3);
    ($start,$stop)=(-$start,-$stop) if($start<0);
    ($start,$stop)=($stop,$start) if $stop<$start; #be flexible
    push(@anchors,{start=>$start,stop=>$stop,sign=>$sign});
  }
  return @anchors
}

sub getRegions {
  my ($file)=@_;
  die "No file specified?" unless $file;
  my @res;
  if($file=~m/\.stitch$/){
    open(my $fh,$file) or die "Couldn't open stitch sub-file $file";
    while(<$fh>){
      my @dat=split(/\t/);
      push(@res,{file=>$dat[0],length=>$dat[1],start=>$dat[2],stop=>$dat[3]});
    }
  }else{
    unless(-f "$file.len"){
      system("$root/getsegments $file");
    }
    open(my $fh,"$file.len") or die "Couldn't open segment data for $file";
    my $length=<$fh>; #first line is length only
    my $at=1;
    while(<$fh>){
      my ($length,$meta)=m/^(\d+)\t(.*)/;
      push(@res,{length=>$length,start=>$at,stop=>$at+$length});
      $at+=10+$length;
    }
  }
  print "Loaded ".scalar(@res).(scalar(@res)==1 ? " region":" regions")." from $file\n";
  return @res;
}

sub slurp {
  local $/;
  open(my $fh,"@_") or return;
  return <$fh>;
}
