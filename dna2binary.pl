#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Std;
use File::Basename;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

$Getopt::Std::STANDARD_HELP_VERSION=true;
getopts('DCncBrl');
if($opt_h){HELP_MESSAGE();exit(0);}

($filename,$outfile)=@ARGV;

if($filename and -e $filename){
  $inseq=`$root/geneparse.pl -c $filename`;
  my ($basename,$path,$suffix) = fileparse($filename);
  $name="$basename-revcomp";
} else {
  print STDERR "File $filename not found. Waiting for input from stdin.\n" unless !$filename or $filename eq "-";
  while($_=<STDIN>){
    chomp;
    $inseq.=$_;
  }
  $name="stdin";
}
$outfile="-" unless $outfile;
if(!$opt_C){
  open(OUTF,">$outfile");
}else{
  open(OUTF,"|$root/faformat.pl --name=\"$name\" - $outfile");
}

if($opt_r or $inseq=~m/^[10]+$/){
  %bases=("00" => "A", "01" => "C", "10" => "G", "11" => "T");
  while($inseq=~m/([10]{2})/g){
#    print "Parsing $` - $& - $'\n";
    $outseq.=$bases{$1};
  }
  $outseq=lc($outseq) if $opt_l;
  print OUTF "$outseq";
}else{
  $inseq=~s/[ \.a-]/00/gi;
  $inseq=~s/c/01/gi;
  $inseq=~s/[gu]/10/gi;
  $inseq=~s/[t]/11/gi;
  $inseq=~s/[^01]//g unless $opt_n;
  print OUTF "Binary: " unless $opt_c;
  print OUTF "$inseq";
  unless($opt_D){
    print "\n" unless $opt_B;
    $inseq=bin2dec($inseq);
    print OUTF "Decimal: " unless $opt_c;
    print OUTF "$inseq";
  }
}
close(OUTF);
print "\n";

sub main::HELP_MESSAGE {
  print <<ENDTEXT
Usage: $0 [-c] [-d] [-n] [<in file>] [<out file>]

If you don't specify an infile or outfile, stdin/stdout is used.
 -C specifies pretty formatted output (can't imagine why...but ok)
 -c specifies clean output. (ie: no labels)
 -D skip decimal output
 -B skip binary output
 -n don't erase non-nucleotides (default is set blasters to kill)
 -r reverse (binary to bases)
 -l output lowercase
ENDTEXT
    ;
}

sub main::VERSION_MESSAGE {
}

sub bin2dec {
  my $arg=shift;
  my ($pos,$ret)=(0,0);
  foreach (split(//,reverse($arg))){
    $ret+=$_<<$pos;
    $pos++;
  }
  return $ret;
}
