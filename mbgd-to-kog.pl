#!/usr/bin/perl

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

use Getopt::Long;
use Pod::Usage;
use File::Basename;
use Data::Dump qw {dump};

use strict;

BEGIN {
  unshift(@INC,(fileparse($0))[1].'perlmodules');
}
use Murasaki;

my $output=pop(@ARGV);
if(!$output or (-f $output and formatCheck($output))){
  push(@ARGV,$output) if $output;
  $output=">-";
}
pod2usage({-verbose=>1,-exitval=>2,-message=>'Need some input file...'}) if $#ARGV<0;

open(our $outf,">$output") or die "Couldn't open $output for writing.";

foreach my $file (@ARGV){
  open(my $inf,$file);
  my $line=<$inf>;
  chomp $line;
  my $headerline=$line; #keep this around because (as of recently) it occurs again at the end of the file for some dumb reason
  my @cols=split(/\t/,$line);
  unless($cols[$#cols]){
    print STDERR "Removing stupid extra tab from header line.\n";
    pop(@cols);
  }
  my @specs=@cols[1..($#cols-2)];
  print STDERR "Using species: @specs\n";
  while(<$inf>){
    chomp;
    next if m/^$headerline/;
    my @dat=split(/\t/);
    die "Invalid number of dat ($#dat vs $#cols)! $_" unless $#dat==$#cols or $#dat+1==$#cols;
    my @locii=@dat[1..($#cols-2)];
    my $func=$dat[$#cols-1];
    my $gene=$dat[$#cols];
    $gene="MBGD gene" unless $gene;
    print $outf "[$func] $dat[0] $gene\n";
    foreach my $i (0..$#locii){
      my $spec=$specs[$i];
      foreach my $locii (split(/ /,$locii[$i])){
	die "Weird entry: $locii" unless $locii=~m/$spec:(.*)/;
	$locii=$1;
	$locii=~s/MG(\d+)/MG_$1/;
	$locii=~s/MPN(\d+)/MPN_$1/;
	$locii=~s/\(\d+\)//;
	$locii=~s/\.\d+//;
	print $outf "  $spec:  $locii\n";
      }
    }
    print $outf "\n";
  }
}

sub formatCheck {
  my $file=pop;
  open(my $inf,$file) or return undef;
  my ($line1,$line2)=map {readline($inf)} (0,0);
  my @fields=map {scalar(split(/\t/,$line1))} ($line1,$line2);
  return 1 if $line1=~m/^ClusterID/ and ($fields[0]==$fields[1] or $fields[0]==($fields[1]+1));
  return undef;
}

__END__

=head1 NAME

mbgd-to-kog.pl - converts mbgd cluster files to kog files

=head1 SYNOPSIS

mbgd-to-kog.pl [options] <input1> [input2 ... ] [output file]

=head1 OPTIONS

If "output file" exists, and is of the right format, it is considered an input file.

No other options exist yet.
