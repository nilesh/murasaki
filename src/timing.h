/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

////////////////
// timing.h
// defs for timing tools
////////////////

#ifndef __TIMING_H
#define __TIMING_H

#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include <iostream>

#define MILLION_INT 1000000  //it's happened before that i miss a zero. that's just silly
#define MILLION_DOUBLE 1000000.0
#define TIME_DOUBLE(a) ((double)(a.tv_sec)+(double)(a.tv_usec)/MILLION_DOUBLE)

extern bool timing_useHumanTime;

using namespace std;

class Timer {
 public:
  struct timeval tv;
  Timer();
  Timer(int sec,int usec);
  Timer(double t);
  void reset(); //reset timer to now
  double asDouble() const;
  
  friend Timer operator+ (const Timer &a,const Timer &b);
  Timer& operator+=(const Timer &a);
  friend Timer operator- (const Timer &a,const Timer &b);
  Timer& operator-= (const Timer &b);
  friend double diff(const Timer &a,const Timer &b);
  friend Timer operator*(const Timer &a,double factor);
  friend Timer operator*(double factor,const Timer &a);
  friend Timer operator/(const Timer &a,double factor);
  friend Timer operator/(double factor,const Timer &a);
  friend ostream& operator <<(ostream &os,const Timer &obj);
};

string elapsed(Timer a,Timer b);
double diff(const Timer &a,const Timer &b);
string humanTime(double dur);

class Stopwatch {
 public:
  Timer accum;
  Timer runStart;
  bool running;

 Stopwatch() : accum(0,0),runStart(0,0),running(false)  {}

  inline void start(){assert(!running);runStart.reset();running=true;}
  inline void stop(){assert(running);accum+=(Timer()-runStart);running=false;}
  inline string asString() const {return humanTime(TIME_DOUBLE(accum.tv));}
  inline double asDouble() const {return TIME_DOUBLE(accum.tv);}
  friend ostream& operator <<(ostream &os,const Stopwatch &obj){return os << obj.asString();}
};

#endif
