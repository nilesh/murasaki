//compare 2 alignments
/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <memory.h>
#include <getopt.h>
#include <vector>
#include <map>
#include <list>
#include <string.h>
#include <boost/lexical_cast.hpp>
#include <sys/types.h>
#include "dinkymath.h"
#include "itree.h"
#include "alignments.h"

#include <boost/regex.hpp>
#include <boost/filesystem/operations.hpp>

using namespace std;
namespace fs = boost::filesystem;

string program_help();
string program_version();

//function decl
double compareAlignment(Alignment &ref,Alignment &test);
void printResults(vector<vector<double> > &results);

//globals
ProgressTicker ticker(100);

Alignment *unmatched=0;
pair<int,int> keep(-1,-1);
string keepfile;
ulong searchRadius=0,mergeDistance=0;
vector<bool> skipJoin;

int main(int argc,char** argv){
  using boost::lexical_cast;
  using boost::bad_lexical_cast;
  int optc;
  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"help",0,0,'?'},
      {"version",0,0,'v'},
      {"keep",1,0,'k'},
      {"keepfile",1,0,'K'},
      {"radius",1,0,'r'},
      {"join",1,0,'j'},
      {"skipjoin",1,0,'J'},
      {0,0,0,0}
    };
    int longindex=0;
    optc=getopt_long(argc,argv,"?vr:K:k:j:J:",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case 'v': cout << program_version();exit(-1);break;
    case '?': cout << program_help();exit(-1);break;
    case 'k': if(!optarg){
	cerr << "Missing argument for --keep"<<endl;
	exit(-2);
      }else{
	boost::cmatch m;
	if(!boost::regex_match(optarg,m,boost::regex("(\\d+),(\\d+)"))){
	  cerr << "Bad format for --keep (try eg. --keep=3,4)"<<endl;
	  exit(-2);
	}
	try {
	  keep.first=lexical_cast<int>(m[1]);
	  keep.second=lexical_cast<int>(m[2]);
	}catch(bad_lexical_cast& e){
	  cerr << "Bad argument to --keep (need numbers)"<<endl;
	  exit(-2);
	}
      }break;
    case 'K':keepfile=string(optarg);break;
    case 'r':
      try {
	searchRadius=lexical_cast<int>(optarg);
      }catch(bad_lexical_cast& e){
	cerr << "Bad argument to --search (need numbers)"<<endl;
	exit(-2);
      }
      break;
    case 'j':
      try {
	mergeDistance=lexical_cast<int>(optarg);
      }catch(bad_lexical_cast& e){
	cerr << "Bad argument to --join (need numbers)"<<endl;
	exit(-2);
      }
      break;
    case 'J':
      try {
	uint seq=lexical_cast<uint>(optarg);
	if(skipJoin.size()<=seq)
	  skipJoin.resize(seq+1,false);
	skipJoin[seq]=true;
      }catch(bad_lexical_cast& e){
	cerr << "Bad argument to --skipjoin (need numbers)"<<endl;
	exit(-2);
      }
      break;
    default: cout << "Unknown option ("<<optc<<")."<<endl<<program_help();exit(1);break;
    }
  }

  uint alignmentCount=0;
  vector<string> files;
  for(int i=optind;i<argc;i++){
    checkSeqs(argv[i]);
    files.push_back(argv[i]);
    alignmentCount++;
  }
  skipJoin.resize(alignmentCount,false);
  
  vector<Alignment> alignments(alignmentCount);
  unmatched=new Alignment;
  for(uint i=0;i<alignmentCount;i++){
    cout << "Loading anchors "<<i<<": "<<files[i];
    if(skipJoin[i])cout << " (without joins)";
    cout << endl;
    loadAnchors(files[i].c_str(),alignments[i],skipJoin[i] ? 0:mergeDistance);
    cerr << "Loaded "<<alignments[i].anchors.size()<<" anchors."<<endl;
  }

  vector<vector<double> > results(alignmentCount,vector<double>(alignmentCount,0));
  for(uint s=0;s<alignmentCount;s++){
    for(uint t=0;t<alignmentCount;t++){
      if(s==t){
	results[s][t]=1.0;
	continue;
      }
      cerr << "Comparing the bits from alignment "<<s<<" ("<<alignments[s].anchors.size()<<") to alignment "<<t<<" ("<<alignments[t].anchors.size()<<")"<<endl;
      results[s][t]=compareAlignment(alignments[s],alignments[t]);
    }
  }
  cerr << "Done!"<<endl;

  printResults(results);
  
  if(keepfile.length()){
    cerr << "Dumping keep differences to "<<keepfile<<endl;
    ofstream of(keepfile.c_str());
    of << *unmatched;
  }

  return 0;
}

void printResults(vector<vector<double> > &results){
  uint size=results.size();
  cout << "For amount row i, column j:\n  percent of anchors from i found to overlap atleast 1 anchor in j"<<endl;
  cout << "\t";
  for(uint t=0;t<size;t++){
    cout << t;
    if(t!=size-1)
      cout << "\t";
  }
  cout << endl;
  for(uint s=0;s<size;s++){
    cout << s << "\t";
    for(uint t=0;t<size;t++){
      cout.precision(5);
      cout << 100*results[s][t];
      if(t!=size-1)
	cout << "\t";
    }
    cout << endl;
  }
}

double compareAlignment(Alignment &ref,Alignment &test){
  uint inRefOnly=0,inBoth=0,total=ref.anchors.size();
  for(list<Anchor>::iterator i=ref.anchors.begin();i!=ref.anchors.end();++i){
    if(searchRadius ? test.contains(*i,searchRadius):test.contains(*i))
      inBoth++;
    else {
      inRefOnly++;
      if(ref.id==keep.first && test.id==keep.second)
	unmatched->add(*i);
    }
  }
  return ((double)inBoth)/((double)total);
}

string program_help(){
  return string("\
Usage: align-compare [options] alignment1 alignment2\n\
\n\
Options\n\
*Takes an argument (like --searchradius 300 or -r300)\n\
  --searchradius|r = consider two anchors overlapping if they're within N bp\n\
                     of each other.\n\
  --join|j         = joins anchors within N bp of each other\n\
  --skipjoins|J    = don't perform joins on given sequence (0-indexed,\n\
                     can be appiled multiple times)\n\
  --keep|k         = record differences between given pair (eg 3,4)\n\
  --keepfile|K     = output differences to given file\n\
\n\
*Toggles: (just --merge or -b)\n\
  --help|h    = this message\n\
  --version|v = version string\n\
");
}

string program_version(){
  return string("align-compare v0.1");
}

