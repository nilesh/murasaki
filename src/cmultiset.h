/*
Murasaki - multiple genome global alignment program
Copyright (C) 2006-2007 Keio University
(Yasunori Osana & Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

//
// cmultiset: memory-conservative multiset / murasaki project
//


#ifndef __CMULTISET_H__
#define __CMULTISET_H__

#define DEFAULT_INITIALSIZE 1
#define DEFAULT_EXPANDSTEP  1
#define DEFAULT_KEEP_SORTED true

#include <set>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include "exceptions.h"

using namespace std;

// typedef char T;

template <typename T> class cmultiset {
public:
  cmultiset();
  ~cmultiset();

  typedef T *iterator;
  typedef T &reference;
  typedef const T *const_iterator;

  // multiset compatible member fuctions
  iterator insert(T token);             // append a token 
  pair <iterator,iterator> equal_range(T token);

  iterator begin(){return array;}
  iterator end(){return &array[arraysize];}
  int size(){ return arraysize; } 
  bool empty(){ if (arraysize==0) return true; else return false;};

  // incompatible but useful...
  void clear();
  int insert(T token, int pos);         // insert a token 

  void block_append(T* tokens, int num); // insert bunch of tokens
  pair <int, T*> block_read();          // for block read

  int lsearch(T token);             // perform linear search
  int bsearch(T token);             // perform binary search
  int bsearch_position(T token);             // find appropriate position by binary search

  // housekeepers
  void sort();                          // perform quicksort on the array
  int pack();                           // release unused memory area

  // status / preferences
  void set_expand_step(int s){  expandstep  = s; };
  void set_keep_sorted(bool a){ keep_sorted = a; };

  int cap(){ return capacity; }     // return capacity
  bool order(){ return sorted; }    // sorted or not

  // and so on...
  int sanityCheck();
  string asString() const;

  T &operator[](int i) const;
  template <typename Tx> friend ostream& operator<<(ostream &os , const cmultiset<Tx> &v);

  T* array;

protected:
  bool keep_sorted;
  bool sorted;
  int arraysize;
  int capacity;

  int initialsize;
  int expandstep;

  int insert_anyway(T token, int pos);  // insert a token without care of order status

  void expand();
  int nextMatch(T token, int pos);  // find next match        (linear search)
  int prevMatch(T token, int pos);  // find previous match    (linear search)

  iterator indexToIterator(int i){ if (i!=-1) return &array[i]; else return begin();}
  int iteratorToIndex(iterator i){ if (i>=begin() && i<end()) return i-begin(); else return 0; }
};

template <typename T> int compareT(const void* a, const void* b){
  const T aa = *((T*)a);
  const T bb = *((T*)b);

  if (aa<bb) return -1;
  if (bb<aa) return 1;
  return 0;
}

template <typename T> cmultiset<T>::cmultiset(){
  arraysize = 0;
  expandstep  = DEFAULT_EXPANDSTEP;
  initialsize = DEFAULT_INITIALSIZE;
  capacity = initialsize;

  keep_sorted = DEFAULT_KEEP_SORTED;

  array = (T*)malloc(sizeof(T) * initialsize);
  sorted = false;

  //  assert(sanityCheck());
  return;
}

template <typename T> cmultiset<T>::~cmultiset(){
  free(array);
}

template <typename T> void cmultiset<T>::clear(){
  arraysize = 0;
  sorted = false;
}

// ----------------------------------------------------------------------
//  append / insert
// ----------------------------------------------------------------------

// append to the array, then returns the index
template <typename T> T* cmultiset<T>::insert(T token){
  int pos;

  if (sorted && keep_sorted) { 
    // keep it sorted
    pos = bsearch_position(token);
    insert_anyway(token, pos);
  } else {
    // otherwise, just append
    if (arraysize == capacity) expand();
    
    pos = arraysize;
    array[pos] = token;
    arraysize++;
    sorted = false;
  }

  //  assert(cout << "add(" << token << " at " << arraysize-1 << ")" << endl);
  //  assert(sanityCheck());
  return indexToIterator(pos);
}

template <typename T> int cmultiset<T>::insert_anyway(T token, int pos){
  if (pos > arraysize) pos = arraysize; // if pos is too large, let's append.
  //  assert(cout << "insert " << token << " at " << pos << endl);

  if (arraysize == capacity) expand();

  // if (pos != arraysize-1)
  if (pos != arraysize) // is this correct?  seems to be ok.
    for (int i=arraysize; i>pos; i--) array[i] = array[i-1];  // memmove() wasn't good... why? why?

  arraysize++;
  array[pos] = token;

  return arraysize;
}

template <typename T> int cmultiset<T>::insert(T token, int pos){
  arraysize = insert_anyway(token, pos);

  if (sorted){
    sorted = false;
    if (pos==0)
      if(compareT<T>((void*)(array+pos), (void*)(array+pos+1)) <= 0) sorted = true;
    if (pos==arraysize-1)
      if(compareT<T>((void*)(array+pos-1), (void*)(array+pos)) <= 0) sorted = true;
    if (!sorted)
      if (compareT<T>((void*)(array+pos-1), (void*)(array+pos)) <= 0 && compareT<T>((void*)(array+pos), (void*)(array+pos+1)) <= 0) sorted = true;
  }

  //  assert(sanityCheck());
  return arraysize;
}

// block append
template <typename T> void cmultiset<T>::block_append(T* tokens, int num){
  
  // make enough rooms
  if( arraysize+num > capacity ){
    capacity = arraysize+num;
    array = (T*)realloc(array, capacity * sizeof(T));
  }

  // then append
  int dst = arraysize;
  for(int i=0; i<num; i++) array[dst+i]=tokens[i];

  arraysize += num;

  // sort if necessary
  if (sorted){ sorted=false; sort(); }
}

// block read
template <typename T> pair<int, T*> cmultiset<T>::block_read(){
  return pair<int, T*>(arraysize, array);
}

// ----------------------------------------------------------------------
//  housekeepers
// ----------------------------------------------------------------------

template <typename T> void cmultiset<T>::expand(){
  int s;

  // s = expandstep;
  s = capacity /2;
  if ( s==0 ) s = 1;
  if ( s>20 ) s = 20;

  capacity += s;

  array = (T*)realloc(array, capacity * sizeof(T));

  if(array == NULL){ throw MurasakiException("cmultiset: Expand() failed (out of memory?)"); }
  //  assert(cout << "array realloced (new capacity: " << capacity << ")" << endl);
}

template <typename T> void cmultiset<T>::sort(){
  if( !sorted )
    qsort(array, arraysize, sizeof(T), compareT<T>);
  sorted = true;
  //  assert(sanityCheck());
}

template <typename T> int cmultiset<T>::pack(){
  // int cap_prev = capacity;
  
  capacity = arraysize;
  array = (T*)realloc(array, capacity * sizeof(T));  
  if (array == NULL) return -1;

  //  assert( cout << "packed from " << cap_prev << " to " << capacity << endl);
  //  assert(sanityCheck());
  return capacity;
}


// ----------------------------------------------------------------------
//  linear search functions (private)
// ----------------------------------------------------------------------

// find specified token in the array, return the index (linear search)
template <typename T> int cmultiset<T>::lsearch(T token){
  //  assert(cout << "linear search for key: " << token << endl);
  return nextMatch(token, 0);
}

// find next match, return the index (linear search)
template <typename T> int cmultiset<T>::nextMatch(T token, int pos){
  //  assert(sanityCheck());
  //  assert(cout << "find next token " << token << " from " << pos <<" in " << arraysize);
  if (pos >= arraysize || pos < 0 ) return -1;

  for(int i=pos; i<arraysize; i++){
    if (array[i] == token){
      //      assert(cout << " / found at: " << i << endl);
      return i;
    }
  }
  //  assert(cout << "not found :(" << endl);
  return -1;
}

// find previous match, return the index (linear search)
template <typename T> int cmultiset<T>::prevMatch(T token, int pos){
  //  assert(sanityCheck());
  //  assert(cout << "find prev token " << token << " from " << pos <<" in " << arraysize);
  if (pos >= arraysize || pos < 0) return -1;

  for(int i=pos; i>=0; i--){
    if (array[i] == token) {
      //      assert(cout << " / found at: " << i << endl);
      return i;
    }
  }
  //  assert(cout << "not found :(" << endl);
  return -1;
}


// ----------------------------------------------------------------------
//  binary search functions (private)
// ----------------------------------------------------------------------


// perform binary search to find proper insert position
template <typename T> int cmultiset<T>::bsearch_position(T token){
  int max = arraysize-1;
  int min = 0;
  int c, r;

  sort();

  // the head and tail
  if (compareT<T>(&token, &array[0]) < 0) return 0;
  if (compareT<T>(&token, &array[arraysize-1]) > 0) return arraysize;
  
  // or insert into the middle
  do {
    if (max-min == 1){
      if (compareT<T>(&array[min], &token) == 0) return min;
      return max;
    } else {
      c = (min+max)/2;
      r = compareT<T>(&array[c], &token);
      if (r == 0) return c;
      //      assert(cout<< "<" << min <<"[" << c << "]" << max << ":" << r<< ">");
      if (r <  0) min=c;
      else        max=c;
      //      assert(cout<< "(" << min << "," << max << ")");
    }
  } while ( min<max );
  return c;
}

// perform binary search
template <typename T> int cmultiset<T>::bsearch(T token){
  int max = arraysize-1;
  int min = 0;
  int c, r;
  
  sort();

  do {
    if (max-min == 1){
      if (compareT<T>(&array[max], &token) == 0) return max;
      if (compareT<T>(&array[min], &token) == 0) return min;
      //  cout << "possible position is <"<< max <<">";
      return -1;
    } else {
      c = (min+max)/2;
      r = compareT<T>(&array[c], &token);
      if (r == 0) return c;
      //      assert(cout<< "<" << min <<"[" << c << "]" << max << ":" << r<< ">");
      if (r <  0) min=c;
      else        max=c;
      //      assert(cout<< "(" << min << "," << max << ")");
    }
  } while ( min<max );
  return -1;
}

// ----------------------------------------------------------------------
//  equal_range()
// ----------------------------------------------------------------------

// binary search version
template <typename T> pair <T*, T*> cmultiset<T>::equal_range(T token){

  int p = bsearch(token);
  if ( p == -1 ) return pair<iterator,iterator>(NULL, NULL);


  int b = p;
  int e = p+1;

  while ( 0 < b )
    if (!( array[b-1] < token )) b--; else break;

  while ( e < arraysize )
    if (!( token < array[e] )) e++; else break;

  //  cout << "[" << b << "," << e << "]";

  T* bb = indexToIterator(b); 
  T* ee = indexToIterator(e);
 
  return pair<iterator,iterator>(bb, ee); 
}


// linear-search version. slow...
/*
template <typename T> pair <T*, T*> cmultiset<T>::equal_range(T token){
  sort();

  int b = 0;
  int e = 1;

  while ( b < arraysize ) 
    if (array[b] < token) b++; else break;

  if (b == arraysize) b=e=-1;
  else{
    e = b;
    while (e < arraysize)
      if (!(token < array[e])) e++; else break;
    
  }
  if (e > arraysize) e = arraysize;

  // assert(cout << "[" << b << "," << e << "]");

  T* bb = indexToIterator(b); 
  T* ee = indexToIterator(e);
 
  return pair<iterator,iterator>(bb, ee); 
}
*/

// ----------------------------------------------------------------------
//  functions for tests
// ----------------------------------------------------------------------


template <typename T> string cmultiset<T>::asString() const{
  ostringstream s;

  for (int i=0; i<arraysize; i++){
    s << array[i] << " ";
  }
  
  return s.str();
}

template <typename T> int cmultiset<T>::sanityCheck(){
  //for(int i=0; i<arraysize; i++)
  //    cout << array[i] << " ";
  cout << asString();

  if (sorted) cout << "/ sorted";
  else cout << "/ not sorted";

  cout << " / size :" << arraysize;
  cout << " / capacity :" << capacity << endl;

  return true;
}

// ----------------------------------------------------------------------
//  operators
// ----------------------------------------------------------------------

template <typename T> ostream& operator<<(ostream &os,const cmultiset<T> &v){
  return os << v.asString();
}

template <typename T> T &cmultiset<T>::operator[](int i) const{
  return array[i];
}

#endif
