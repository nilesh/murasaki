/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// seqread.cc
// c++ support for reading various sequence formats
//////////////

#include "genopts.h"
#include "seqread.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <err.h>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/lexical_cast.hpp>

#ifdef WITH_MINGW
#include "mingw32compat.h"
#endif


using namespace std;
namespace fs = boost::filesystem;
namespace io = boost::iostreams;

const char seqDiv[]="NNNNNNNNNN";
const boost::regex stitchLine("(\\S+)\\t(\\d+)\t(\\d+)\\t(\\d+)");

SequenceReaderGlobalOptions* SequenceReaderGlobalOptions::_instance;

SequenceReader::SequenceReader():
  repeatMask(false),lengthOnly(false),
  recordLength(true),silent(false),
  verbose(false)
{}

size_t SequenceReader::readSeqInto(string& dst,const string &filename){
  SequenceFileReader reader(filename,options());
  char c;
  while((c=reader.getc())){
    dst.append(1,c);
  }
  return reader.size();
}

size_t SequenceReader::readSeqInto(string& dst,istream& is,const string filename){
  SequenceFileReader reader(is,filename,options());
  char c;
  while((c=reader.getc())){
    dst.append(1,c);
  }
  return reader.size();
}

SequenceByteReader::SequenceByteReader(istream& _is,const string _filename,int options) :
  repeatMask(options & SEQO_RMASK),lengthOnly(options & SEQO_LEN),recordLength(!(options & SEQO_NOREC)),silent(options & SEQO_SILENT),verbose(options & SEQO_VERBOSE),
  bufsize(4096),linenum(1),
  finished(false),filetype(SEQFILE_END),
  filename(_filename),is(_is),
  redirect(0),count(0),subcount(0),
  subSeqId(0),
  bogusCharWarned(false)
{
  if(!is)
    throw SeqReadException("Couldn't read from "+filename);
  
  boost::regex rawre("[acgtnACGTN]+\\n?");
  boost::cmatch results;
  char buf[bufsize];
  is.getline(buf,bufsize);
  if((is.eof() || (is.fail() && is.gcount()>=(bufsize-1))) || regex_match(buf,results,rawre)){//appears to be a raw sequence. read all and dump
    is.clear();
    if(verbose)cerr << "It's a raw file!"<<endl;
    filetype=SEQFILE_RAW;
    buffers.push_back(string());
    buffers.back().reserve(bufsize);
    procBuf(buffers.back(),buf,bufsize);
    return;
  }//end raw file parsing

  if(is.bad())
    throw SeqReadException("Failed to read first line");

  if(buf[0]=='>' || buf[0]==';'){ //it's a fasta file!
    if(verbose)cerr << "It's a fasta file!"<<endl;
    filetype=SEQFILE_FASTA;
    while(buf[0]!='>' && !is.fail()){ //just in case some jerk starts a file with comments
      is.getline(buf,bufsize);
      linenum++;
    }
    if(buf[0]!='>')
      throw SeqReadException("Never found the beginning of the fasta file.");
    //any further occurences of > mean we insert 10 N's
    subSeqName=string(buf+1);
    return;
  } //end of fasta file parsing.

  if(regex_match(buf,results,stitchLine)){
    if(verbose)cerr << "It's a stitch file!"<<endl;
    filetype=SEQFILE_STITCH;
    subSeqName=results[1];
    addRedirect(results);
    return;
  }
  if(verbose)cerr << "It doesn't look like anything else, so assuming it's a genebank file."<<endl;
  filetype=SEQFILE_GBK;
  const boost::regex originLine("ORIGIN\\s*[^\\w]?");
  do {
    is.getline(buf,bufsize);
    linenum++;
    if(!is.good())
      throw SeqReadException("file ended without finding the ORIGIN line");
  }while(!regex_match(buf,results,originLine));
  if(verbose)cerr << "Found ORIGIN on line "<<linenum<<endl;
  return;
}

inline void SequenceByteReader::procBuf(string &dst,const char *buf,const int &bufsize){
  if(repeatMask)
    for(const char *i=buf,*stop=buf+bufsize;i!=stop;i++){
      if(!*i)
	break;
      char c=*i;
      switch(c){
      case 'a':case 'c': case 'g': case 't': c='n';
      case 'A':case 'C': case 'G': case 'T': case 'N': case 'n':
	dst.append(1,c);
	break;

      case 'b': case 'd': case 'e': case 'f': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'B': case 'D': case 'E': case 'F': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': 
	if(!bogusCharWarned){
	  bogusCharWarned=true;
	  warnx("Warning: %s contains illegal (non [acgtn]) characters (%c at %ld). %s",filename.c_str(),c,(long)size(),(SequenceReaderGlobalOptions::options()->ignoreBogusChars ? "Ignoring.":"Converting to Ns."));
	}
	if(!SequenceReaderGlobalOptions::options()->ignoreBogusChars)
	  dst.append(1,'n');
      }
    }
  else
    for(const char *i=buf,*stop=buf+bufsize;i!=stop;i++){
      if(!*i)
	break;
      switch(*i){
      case 'a':case 'c': case 'g': case 't': case 'n':
      case 'A':case 'C': case 'G': case 'T': case 'N':
	dst.append(1,*i);
	break;

      case 'b': case 'd': case 'e': case 'f': case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'o': case 'p': case 'q': case 'r': case 's': case 'u': case 'v': case 'w': case 'x': case 'y': case 'z': case 'B': case 'D': case 'E': case 'F': case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'O': case 'P': case 'Q': case 'R': case 'S': case 'U': case 'V': case 'W': case 'X': case 'Y': case 'Z': 
	if(!bogusCharWarned){
	  bogusCharWarned=true;
	  warnx("Warning: %s contains illegal (non [acgtn]) characters (%c at %ld (line %ld)). %s",filename.c_str(),*i,(long)size(),(long)linenum,(SequenceReaderGlobalOptions::options()->ignoreBogusChars ? "Ignoring.":"Converting to Ns."));
	}
	if(!SequenceReaderGlobalOptions::options()->ignoreBogusChars)
	  dst.append(1,'n');	  
      }
    }
}

bool SequenceByteReader::readMore(){
  buffers.push_back(string());
  buffers.back().reserve(bufsize);
  string &dst=buffers.back();
  if(is.eof()){
    return false;
  }

  char buf[bufsize];
  bzero(buf,bufsize);
  switch(filetype){
  case SEQFILE_RAW:
    is.read(buf,bufsize);
    if(is.bad())
      throw SeqReadException("Read() failed mid raw-stream");
    procBuf(dst,buf,bufsize);
    break;
  case SEQFILE_FASTA:
    do {
      is.getline(buf,bufsize);
      //      cout << "Read fasta line "<<linenum<<": "<<buf<<endl;
      if(is.bad())
	throw SeqReadException("read() failed mid-fasta file");
      if(buf[0]=='>'){
	procBuf(dst,seqDiv,11);
	subSeqId++;
	subSeqName=string(buf+1);
	continue;
      }
      if(buf[0]==';')continue; //comment line
      //otherwise it must be content
      procBuf(dst,buf,bufsize);
      if(is.eof())
	return true;
      if(!is.fail())
	linenum++;
      else
	is.clear(); //just an overly full line
      break;
    }while(1);
    break;
  case SEQFILE_STITCH:
    is.getline(buf,bufsize);linenum++;
    if(is.fail()) //most likely already at eof
      return false;
    if(!is.good())
      throw SeqReadException("read() failed mid stitch file");
    if(1){//purely to scope results
      boost::cmatch results;
      if(!regex_match(buf,results,stitchLine))
	throw SeqReadException("stitch file suddenly contained a non-stitch line");
      //ok, can add a new sequence.
      dst.append(seqDiv);
      addRedirect(results);
      subSeqId++;
      subSeqName=results[1];
    }
    return true;
    break;
  case SEQFILE_GBK:
    is.getline(buf,bufsize);linenum++;
    if(is.fail()) //most likely already at eof
      return false;
    if(!is.good())
      throw SeqReadException("read() failed mid sequence");
    procBuf(dst,buf,bufsize);
    return true;
    break;
  default:
    throw SeqReadException("Read on undefined file format");
  }
  return true;
}

void SequenceByteReader::addRedirect(boost::cmatch results){
  assert(redirect==0);
  fs::path stitchPath;
  if(!filename.empty()){//commence the annoying parsing of the filename!
    fs::path stitchfile(filename);
    stitchPath=stitchfile.branch_path();
  }
  if(verbose)cerr << results[2] << " region from "<< results[3] << " to "<<results[4]<<" is "<<results[1]<<endl;
  if(!lengthOnly){
    try {
      fs::path subpath(results[1]);
      fs::path relative(stitchPath / subpath);
      string usePath(fs::exists(relative) ? relative.string():string(results[1]));
      redirect=new SequenceFileReader(usePath,options());
    }catch(SeqReadException e){
      throw SeqReadException("Reading subsequence "+string(results[1])+" failed: "+e.reason());
    }
  }else{
    using namespace boost;
    try {
      count+=lexical_cast<size_t>(results[2]); //assume we read the whole thing...
    }catch(bad_lexical_cast& e){
      throw SeqReadException("Invalid stitch member length: "+results[2]+" :"+e.what());
    }
  }
}

char SequenceByteReader::getc(){
  if(finished)return 0; //already finished on previous pass

  if(!buffers.empty()){ //do i have any unsent data pending? if so, send that
    if(subcount<buffers.front().length()){
      count++;
      assert(buffers.front()[subcount]);
      return buffers.front()[subcount++];
    }else{
      buffers.erase(buffers.begin());
      subcount=0;
    }
  }
  if(redirect){ //am i part way through stitch file?
    char c=redirect->getc();
    if(c){
      count++;
      return c;
    }else
      redirect=0;
  }
  while(readMore()){ //read more out of our file (which might be blank lines, thus the while)
    //file's not over yet, so return that
    assert(!buffers.empty());
    if(buffers.front().length()){ //real data. safe to return.
      count++;
      assert(buffers.front()[subcount]);
      return buffers.front()[subcount++];
    }//if not, try the next line.
    else
      buffers.erase(buffers.begin()); //but still have to erasethis empty junk buffer!
  }
  //end of file
  if(verbose)cerr << "End of file ("<<filename<<") encountered. Length: "<<count<<endl;
  finished=true;
  if(!filename.empty())
    writeBackSize();
  return 0;
}

bool SequenceByteReader::writeBackSize(){
  if(recordLength){
    fs::path lengthFile(filename+string(".length"));
    if(!fs::exists(lengthFile) || fs::last_write_time(lengthFile)<fs::last_write_time(filename)){
      if(verbose)cerr << "Writing out "<<lengthFile.string()<<endl;
      ofstream fh(lengthFile.string().c_str());
      if(fh)
	fh << count;
      else
	if(!silent)
	  cerr << "Warning: couldn't record "<<filename<<"'s length to "<<lengthFile.string()<<endl;
      return true;
    }
  }
  return false;
}

bool SequenceByteReader::eof(){
  return finished;
}

bool SequenceFileReader::parseRangeSpec(string &filename,pair<size_t,size_t> &range,int options){
  using namespace boost;
  static const regex rangeSpec("^(.*)[\\(\\{\\[](\\d+)\\W(\\d+)[\\)\\}\\]]$");
  smatch results;
  if(regex_match(filename,results,rangeSpec)){
    if(options & SEQO_VERBOSE)
      cerr << "Selecting range ["<<results[2]<<","<<results[3]<<"] of "<<results[1]<<endl;
    filename=results[1];
    try {
      range.first=lexical_cast<size_t>(results[2])-1; //offset by put in 0-based coords
      range.second=lexical_cast<size_t>(results[3])-1;
      if(range.first>range.second)
	swap(range.first,range.second);
    }catch(bad_lexical_cast& e){
      throw SeqReadException(string("Bad value in range specification (")+results[0]+"):"+e.what());
    }
    return true;
  }else
    return false;  
}

bool SequenceFileReader::useRangeSpec(int noOpen){
  if(parseRangeSpec(filename,range,options)){
    if(ifs)
      ifs.close();
    if(!noOpen){
      ifs.open(filename.c_str());
    }
    rangeOnly=true;
    return true;
  }else
    return false;
}

SequenceFileReader::SequenceFileReader(const string _filename,int _options) :
  options(_options),
  filename(_filename),ifs(_filename.c_str()),is(NULL),
  byteReader(NULL),
  rangeOnly(false),range(0,0),outCount(0)
{
  useRangeSpec();
  if(!ifs)
    throw SeqReadException("Couldn't open file: "+filename);
  is=setupInputFilters(ifs);
  if(!is)
    throw SeqReadException("Failed to set up input filters");
  byteReader=new SequenceByteReader(*is,filename,_options);
}

SequenceFileReader::SequenceFileReader(istream &_is,const string _filename,int _options) :
  options(_options),
  filename(_filename),ifs(),is(setupInputFilters(_is)),
  byteReader(NULL),
  rangeOnly(false),range(0,0),outCount(0)
{
  useRangeSpec(1);
  if(!_is)
    throw SeqReadException("Invalid input stream");
  if(!is)
    throw SeqReadException("Failed to set up input filters");
  byteReader=new SequenceByteReader(*is,filename,_options);
}

SequenceFileReader::~SequenceFileReader(){
  if(is)
    delete is;
  if(byteReader)
    delete byteReader;
}

istream* SequenceFileReader::setupInputFilters(istream &file){
  if(!file)
    return NULL; //don't create dud filters
  if(!filename.empty()){ //if it's not actually a file, we can't seek.
    unsigned char magic_c[2];
    file.read((char*)magic_c,2);
    file.seekg(0);
    unsigned magic=(magic_c[0]<<8) | magic_c[1];
    switch(magic){
    case 0x425a:
      in.push(io::bzip2_decompressor());break;
    case 0x1f8b:
      in.push(io::gzip_decompressor());break;
    case 0x1f9d:
      in.push(io::zlib_decompressor());break;
    }
  }
  in.push(file);
  return new istream(&in);
}

size_t SequenceFileReader::peekLength() const {
  return getLength(filename,options);
}

size_t SequenceFileReader::getLength(string filename,int options){
  pair <size_t,size_t> range;
  bool rangeOnly=parseRangeSpec(filename,range,options);
  size_t length;
  if(!fs::exists(filename))
    throw SeqReadException(string("Sequence file not found: ")+filename); 
  fs::path lengthFile(filename+string(".length"));
  if(!fs::exists(lengthFile) ||
     (fs::exists(filename) && fs::last_write_time(lengthFile)<fs::last_write_time(filename))){
    if(options & SEQO_VERBOSE)
      cerr << "Length file out of date. Calculating..."<<endl;
    //needs to be freshened
    SequenceFileReader reader(filename,options | SEQO_LEN);
    length=reader.readLength();
  }else{
    ifstream is(lengthFile.string().c_str());
    if(!is){
      throw SeqReadException(string("Couldn't open length file:")+lengthFile.string());
    }
    size_t size;
    is >> size;
    length=size;
    is.close();
  }
  if(rangeOnly)
    return min<size_t>(length,range.second+1)-range.first;
  else
    return length;
}

char SequenceFileReader::getc(){
  if(rangeOnly){
    if(byteReader->size()<range.first && (options & SEQO_VERBOSE))
      cerr << "Looking for start of range: "<<range.first<<endl;
    while(byteReader->size()<range.first)
      byteReader->getc();
    if(byteReader->size()>range.second){
      if(options & SEQO_VERBOSE)cerr << "End of subrange"<<endl;
      return 0;
    }
  }
  char c=byteReader->getc();
  if(c)outCount++;
  return c;
}

size_t SequenceFileReader::readLength(){
  while(byteReader->getc())
    ;
  return byteReader->size();
}

size_t SequenceFileReader::size(){
  return outCount;
}

bool SequenceFileReader::eof(){
  if(!rangeOnly)
    return byteReader->eof();
  return byteReader->eof() || byteReader->size()>range.second;
}

string SequenceFileReader::formatString(){
  switch(format()){
    case SEQFILE_RAW: return "raw";break;
    case SEQFILE_STITCH: return "stitch";break;
    case SEQFILE_FASTA: return "fasta";break;
    case SEQFILE_GBK: return "genebank";break;
    default: return "unknown";break;
  }
}

SequenceReaderGlobalOptions* SequenceReaderGlobalOptions::options(){
  if(!_instance)
    _instance=new SequenceReaderGlobalOptions();
  return _instance;
}

SequenceReaderGlobalOptions::SequenceReaderGlobalOptions():
  ignoreBogusChars(false)
{
  if(getenv("MURASAKI_SR_IGNOREBOGUS"))
    getYesNo(getenv("MURASAKI_SR_IGNOREBOGUS"),ignoreBogusChars,"MURASAKI_SR_IGNOREBOGUS");
}
