#ifndef __CRYPTOHASHER_H__
#define __CRYPTOHASHER_H__

#include "globaltypes.h"
#include "sequence.h"
#include <vector>

class CryptoHasher {
protected:
public:
  virtual word digest(const vector<word>words, BitSequence *pat)=0;
};

#ifdef USE_LIBCRYPTOPP
//using Crypto++ library
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h> //fastest
#include <cryptopp/sha.h> //medium
#include <cryptopp/whrlpool.h> //slow
//Non-cryptographic checksums
#include <cryptopp/crc.h>
#include <cryptopp/crc.h>
#include <cryptopp/adler32.h>

template<typename T>
class GenCryptoHasher : public CryptoHasher {
protected:
  T hashfunc;
public:
  virtual word digest(const vector<word> words, BitSequence *pat){
    size_t buflen=pat->wordCount()*sizeof(word);
    byte bytes[buflen];
    for(size_t i=0;i<pat->wordCount();i++){
      word *dst=((word*)(bytes))+i;
      *dst=(words[i] & pat->readWord(i));
    }
    byte digest[T::DIGESTSIZE];
    hashfunc.CalculateDigest(digest,bytes,buflen);
    return *((word*)digest);//first word, if it's a good cryptographic function, should be as good as any other word (probably?)...
  }
};

typedef GenCryptoHasher<CryptoPP::Weak::MD5> MD5CryptoHasher;
typedef GenCryptoHasher<CryptoPP::SHA1> SHACryptoHasher;
typedef GenCryptoHasher<CryptoPP::Whirlpool> WhirlpoolCryptoHasher;
typedef GenCryptoHasher<CryptoPP::CRC32> CRC32CryptoHasher;
typedef GenCryptoHasher<CryptoPP::Adler32> Adler32CryptoHasher;
#endif

#endif
