#ifndef _GENOPTS_H_
#define _GENOPTS_H_

bool isYes(const char* str);
bool isNo(const char* str);
bool getYesNo(const char* str,bool &toggle,const char* name);
bool toggleYesNo(const char* str,bool &toggle,const char* name);

template <typename T>
bool getLexical(const char* str,T &ref,const char* name);

#endif
