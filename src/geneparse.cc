/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// geneparse.cc
// c++ implementation of the geneparse.pl program
//////////////

#include <iostream>
#include <fstream>
#include <assert.h>
#include "seqread.h"
#include <getopt.h>

using namespace std;

string program_version();
string program_help();

bool toUpper=false;

char baseMorph(char c) {
  if(!toUpper)
    return c;
  if(c>='a' && c<='z')
    return c-'a'+'A';
  return c;
}

int main(int argc,char **argv){
  SequenceReader reader;
  bool clean=false;
  bool verbose=false;
  bool typeOnly=false;
  bool fatalErrors=false;

  ostream* os=&cout;
  string customOutput;

  int optc;
  while(1){
    //options struct:
    // name, has_arg, store_pointer, return_value
    static struct option long_options[] = {
      {"repeatmask",0,0,'r'},
      {"length",0,0,'l'},
      {"clean",0,0,'c'},
      {"help",0,0,'h'},
      {"version",0,0,'v'},
      {"output",1,0,'o'},
      {"quiet",0,0,'q'},
      {"type",0,0,'t'},
      {"upper",0,0,'U'},
      {"unmask",0,0,'U'},
      {"fatal",0,0,'F'},
      {0,0,0,0}
    };
    int longindex=0;
    string prefreq;
    optc=getopt_long(argc,argv,"trlchvVo:UF",long_options,&longindex);
    if(optc==-1)break;
    switch(optc){
    case 'r': reader.repeatMask=true;break;
    case 'l': reader.lengthOnly=true;break;
    case 'h': cout << program_help();exit(0);break;
    case 'V': cout << program_version();exit(0);break;
    case 'v': verbose=true;reader.verbose=true;break;
    case 'q': reader.silent=true;break;
    case 'c': clean=true;break;
    case 'o': os=new ofstream(optarg);
      if(!os->good()){
	cerr << "Couldn't open "<<optarg<<" for writing."<<endl;
	exit(-10);
      }
      customOutput=string(optarg);
      clean=true;
      break;
    case 't': typeOnly=true;reader.lengthOnly=true;break;
    case 'U': toUpper=true;break;
    case 'F': fatalErrors=true;break;
    default: cerr << "Unknown option: "<<(char)optc<<endl; assert(0);break;
    }
  }

  string seq;
  size_t count=0;
  if(argc<=optind){
    //read from stdin
    try {
      size_t size=0;
      SequenceFileReader filereader(cin);
      if(typeOnly){
	*os << filereader.formatString();
	goto DONE;
      }
      while(char c=baseMorph(filereader.getc())){
	if(!reader.lengthOnly)
	  *os << c;
	size++;
      }
      if(verbose)cerr << "Length: "<<size<<endl;
      count+=size;
    }catch(SeqReadException e){
      cerr << "Error reading from stdin: "<<e.reason()<<endl;
      if(fatalErrors)
	exit(-1);
    }
  }else
    for(int i=optind;i<argc;i++){
      seq.clear();
      string file(argv[i]);
      size_t size;
      if(reader.lengthOnly && !typeOnly){
	try{
	  size=SequenceFileReader::getLength(file,reader.options());
	  count+=size;
	}catch(SeqReadException e){
	  cerr << "Error getting length for "<<file<<": "<<e.reason()<<endl;
	  if(fatalErrors)
	    exit(-1);
	}
	continue;
      }
      if(verbose)cerr << "Reading "<<file<<endl;
      try {
	SequenceFileReader filereader(file,reader.options());

	if(typeOnly){
	  *os << filereader.formatString();
	  if(optind<argc-1){
	    if(clean)
	      *os << " ";
	    else
	      *os << endl;
	  }
	  continue;
	}

	while(char c=baseMorph(filereader.getc())){
	  *os << c;
	}
	size=filereader.size();
      }catch(SeqReadException e){
	cerr << "Error reading "<<file<< ": "<<e.reason()<<endl;
	if(fatalErrors)
	  exit(-1);
	continue;
      }
      if(verbose)cerr << "Length: "<<size<<endl;
      count+=size;
    }

  if(reader.lengthOnly && !typeOnly)
    *os << count;

 DONE:
  if(!clean)
    *os << endl;
  if(!customOutput.empty())
    delete os;
  return 0;
}

string program_help(){
  return string("\
Usage: geneparse [options...] [input] [input2 ...]\n\
\n\
A specific range within an input file can be specified by file[start,stop].\n\
The square brackets can be interchanged for any of {[()]}, (eg.\n\
\"genome.fa[3000,4000]\" or \"genome.fa{3000~4000}\").\n\
Be aware that all of those might be parsed by your shell.\n\
Also any non-word character can be used to separate the numbers.\n\
\n\
Options:\n\
  --repeatmask|-r   - use soft-repeatmasked sequences (ie: replace\n\
                      lowercase bases with N's).\n\
  --upper|--unmask|-U - uppercase all bases. \n\
  --length|-l       - just print the length and exit\n\
  --clean|-c        - don't append a new line when finished\n\
  --fatal|-F        - errors are fatal\n\
  --version|-V      - program version\n\
  --help|-h         - this message\n\
  --output|-o       - send output to a file (otherwise use stdout)\n\
                      (--output implies --clean)\n\
  --quiet|-q        - silence all warnings\n\
  --verbose|-v      - lots of extra details\n\
");
}
 
string program_version(){
  return string("0.2");
}
