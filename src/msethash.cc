/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

/////////////////////////
// the original multiset hash buckets
//////////////////////

#include "sequence.h"
#include "msethash.h"
#include "murasaki.h"
#include <set>

using namespace std;

MSetHash::MSetHash(BitSequence *pat)
  : Hash(pat),
    activeRange((HashIte)NULL,(HashIte)NULL)
{
  fasta=new MHash[hash_size];
  memset(fasta,0,sizeof(MHash)*hash_size);
}

void MSetHash::clear(){
  for(word i=0; i<hash_size; i++) delete fasta[i];
  memset(fasta,0,sizeof(MHash)*hash_size);
}

void MSetHash::add(const HashKey key,const HashVal &val){
  if(!fasta[key])
    fasta[key]=new multiset<HashVal>;
  fasta[key]->insert(val);
}

void MSetHash::getMatchingSets(HashKey key,list<LocList> &sets){
  if(emptyAt(key))
    return;

  HashIte start(fasta[key]->begin()),stop(fasta[key]->end());
  for(HashIte si=start;si!=stop;){
    sets.push_back(LocList(seq_count));
    LocList &locList=sets.back();
    for(HashIte setEnd(fasta[key]->upper_bound(*si));si!=setEnd;++si){
      locList[si->seqId()].push_back(*si);
    }
  }
}

void MSetHash::lookup(HashKey key,LocList &locList){
  for(
      pair<HashIte,HashIte> range(fasta[key]->begin(),
				  fasta[key]->end());
      range.first!=range.second;
      range.first++){
    locList[range.first->seqId()].push_back(*range.first);
  }    
}

bool MSetHash::emptyAt(const HashKey key){
  if(!fasta[key] || fasta[key]->empty())
    return false;
  return sizeAt(key)==0;
}

word MSetHash::sizeAt(const HashKey key){
  if(!fasta[key]) return 0;
  else return fasta[key]->size();
}

word MSetHash::sizeAt(const HashKey key,const HashVal &val){
  return fasta[key]->count(val);
}
