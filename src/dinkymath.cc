/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

///////////////
//dinkymath.cc
//for all those math functions you want all over and hate rewriting
///////////

#include "dinkymath.h"
#include "murasaki.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <iterator>
#include <stdlib.h>

using namespace std;

string littleEndianStr(unsigned int n,int digits){
  char str[200];
  unsigned int mask=1;
  int last=0;
  for(int i=0;mask;i++,mask<<=1){
    str[i]=(n & mask) ? '1':'0';
    if(n & mask)
      last=i;
  }
  str[digits]=0;
  return string(str);
}

string littleEndianPat(unsigned int n,int digits){
  char str[200];
  unsigned int mask=1;
  int last=0;
  for(int i=0;mask;i++,mask<<=1){
    str[i]=(n & mask) ? '1':' ';
    if(n & mask)
      last=i;
  }
  str[digits]=0;
  return string(str);
}

string littleEndianStr(unsigned int n){
  return littleEndianStr(n,32);
}

template <class T>
int writeOutVector(string filename,const vector<T> &v,string delim,int from,int to,int skipZeros){
  ofstream os(filename.c_str());
  for(int i=from;i<to;i++)
   {
     if(v[i] || !skipZeros)
       os << i << delim << v[i] << endl;
   }
  return 0;
}

string dstring(int i){
  char buf[50];
  sprintf(buf,"%d",i);
  return string(buf);
}

string dstring(long i){
  char buf[50];
  sprintf(buf,"%ld",i);
  return string(buf);
}

string fstring(float f){
  char buf[50];
  sprintf(buf,"%.2f",f);
  return string(buf);
}

string dhstring(int i){
  char buf[50];
  sprintf(buf,"%#08x",i);
  return string(buf);
}

string humanMemory(long bytes){
  return humanMemory((unsigned long) bytes);
}

string humanMemory(unsigned int bytes){
  return humanMemory((unsigned long) bytes);
}
string humanMemory(int bytes){
  return humanMemory((unsigned long) bytes);
}

string humanMemory(unsigned long b){
  char buf[64];
  double f=(double)b;
  if(b==1)
    return string("1 byte");
  if(b<1024)
    sprintf(buf,"%lu bytes",b);
  else if(b<1024*1024)
    sprintf(buf,"%.2lf KB",f/1024);
  else if(b<1024*1024*1024)
    sprintf(buf,"%.2lf MB",f/1024/1024);
  else if(f<1024.*1024.*1024.*1024.)
    sprintf(buf,"%.2lf GB",f/1024/1024/1024);
  else
    sprintf(buf,"%.3lf TB",f/1024/1024/1024/1024);
  return string(buf);
}

string humanMemory(double f){
  char buf[64];
  if(f==1.)
    return string("1 byte");
  if(f<1024)
    sprintf(buf,"%f bytes",f);
  else if(f<1024.0*1024.0)
    sprintf(buf,"%.1f KB",f/1024.0);
  else if(f<1024.0*1024.0*1024.0)
    sprintf(buf,"%.1f MB",f/1024.0/1024.0);
  else if(f<1024.*1024.*1024.*1024.)
    sprintf(buf,"%.1f GB",f/1024./1024./1024.);
  else if(f<1024.*1024.*1024.*1024.*1024.)
    sprintf(buf,"%.3f TB",f/1024./1024./1024./1024.);
  else
    sprintf(buf,"%.3e bytes",f);
  return string(buf);
}

string humanScale(double f){
  char buf[64];
  if(f==1.)
    return string("1");
  if(f<1024)
    sprintf(buf,"%f",f);
  else if(f<1e3)
    sprintf(buf,"%.1fK",f/1e3);
  else if(f<1e6)
    sprintf(buf,"%.1fM",f/1e6);
  else if(f<1e9)
    sprintf(buf,"%.1fG",f/1e9);
  else if(f<1e12)
    sprintf(buf,"%.3fT",f/1e12);
  else
    sprintf(buf,"%.3e",f);
  return string(buf);
}

ProgressTicker::ProgressTicker(size_t _max):
  max(_max),lineDelay(max/(size_t)10),dotDelay(max/(size_t)100),
  lineCount(0),dotCount(0),lastCall(0)
{ }

void ProgressTicker::reset(size_t _max){
  lastCall=0;
  max=_max;
  lineDelay=max/10;
  dotDelay=max/100;
  lineCount=0;
  dotCount=0;
  started.reset();
}

size_t ProgressTicker::tick(size_t ini){
  while(lastCall<ini){
    lastCall++;
    int percent=0;
    if(!lineCount--){
      dotCount=dotDelay;
      lineCount=lineDelay;
      //while this may shave a few cycles, it's not worth people going "wtf? why does it say 104%??" ("because 104% was just an estimate")
      //      percent=dotDelay==0 ? lastCall*(size_t)100/max:(lastCall/dotDelay);
      percent=lastCall*(size_t)100/max;
      char buf[50];
      sprintf(buf,"%2d%%",percent);
      if(percent==0)
	cout << buf;
      else {
	Timer now;
	double elapsed_d=diff(now,started);
	double expTotal=elapsed_d/((double)percent/100.0);
	double remaining=expTotal-elapsed_d;
	time_t eta=(time_t)(started.asDouble()+expTotal);
	cout << " (T -"<<humanTime(remaining)<<") ETA: "<<ctime(&eta) << buf;
      }
      cout.flush();
      return percent; 
    }
    if(!dotCount--){
      percent=dotDelay==0 ? lastCall*(size_t)100/max:(lastCall/dotDelay);
      dotCount=dotDelay;
      cout << ".";
      cout.flush();
      return percent;
    }
  }
  lastCall=ini;
  return -1; //no tick
}

void ProgressTicker::done(){
  cout << endl;
}

size_t progressTick(size_t i,size_t max){
  static int lineDelay=max/10,dotDelay=max/100,
    lineCount=0,dotCount=0;
  int percent=dotDelay==0 ? i*100/max:(i/dotDelay);
  if(!lineCount--){
    dotCount=dotDelay;
    lineCount=lineDelay;
    char buf[50];
    sprintf(buf,"%2d%%",percent);
    cout << endl << buf;
    cout.flush();
    return percent; 
  }
  if(!dotCount--){
    dotCount=dotDelay;
    cout << ".";
    cout.flush();
    return percent;
  }
  return -1; //no tick
}

RandomTraverse::RandomTraverse(int _min,int _max) :
  min(_min),max(_max)
{
  srand(time(0));

  int mid=(min+max)/2;
  for(int i=0;i<mid;i++){
    hit.insert(mid+i);
    hit.insert(mid-i);
  }
  hit.insert(min);
  hit.insert(max);
}

int RandomTraverse::next(){
  if(hit.empty())return -1;

  int n=rand()%(*(--hit.end())-min)+min;
  if(!hit.count(n)){
    set<int>::iterator next=hit.upper_bound(n);
    assert(next!=hit.end());
    n=*next;
  }
  hit.erase(n);
  return n;
}

int RandomTraverse::remaining(){
  return hit.size();
}

void writeOut(string filename,string content,bool append){
  ofstream outf(filename.c_str(),append ? ios_base::out:(ios_base::out | ios_base::app));
  outf << content;
}

unsigned gcd(unsigned a,unsigned b){
  if(a==0 && b==0)
    return 0; //handy definition...
  unsigned t;
  while(b){
    t=b;
    b=a%b;
    a=t;
  }
  return a;
}

unsigned lcm(unsigned a,unsigned b){
  return a*b/gcd(a,b);
}

string percent(double a,double b){
  char buf[128];
  sprintf(buf,"%f%%",a/b*100.0);
  return string(buf);
}

EtaBar::EtaBar(int _max): 
  max(_max),
  updateDelay(_max/bercent), //hey, we can eliminate mod/division by using powers of 2 like 128 (wee) (for sanity's sake we can write /128 because it gets optimized to >>7 by the compiler. We'll this Bercent (get it? binary percent. ahahaha.)
  untilNextUpdate(_max & (bercent-1)),
  lastCall(0),started()
{ } 

int EtaBar::tick(){
  return tick(lastCall+1);
}

int EtaBar::tick(int at){
  int diff=at-lastCall;
  untilNextUpdate-=(diff);
  lastCall=at;
  if(untilNextUpdate<=0){
    untilNextUpdate=updateDelay;
    Timer now;
    cout << "Bercent done: "<<at/bercent<<" Eta: "<<((now-started)/((double)at/(double)max))<<endl;
    return 1;
  }
  return 0;
}

