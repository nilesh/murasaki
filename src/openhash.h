/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

////////
// super-memory-miserly hash list storage
// Kris Popendorf
///////

#ifndef OPENHASH__H
#define OPENHASH__H

#include "ecolist.h"
#include "bitmap.h"
#include "sequence.h"
#include "options.h"

using namespace std;

class OpenHash : public Hash {
 public:
  typedef Ecolist::val_type val_type;

  //abstract parts
  void clear();
  void add(const HashKey key,const HashVal &val);
  void getMatchingSets(HashKey key,list<LocList> &sets);
  void lookup(HashKey key,LocList &locList);//debug only
  bool emptyAt(const HashKey key);
  word sizeAt(const HashKey key);
  word sizeAt(const HashKey key,const HashVal &val);
 
  //for computing memory costs
  static const word linear_cost(word c) {
    return ((word)(Ecolist::seqbits+Ecolist::idxbits))*(c/8);
  }
  static const word bucket_prep_cost(word c)
  {
    return (word)(Ecolist::cost(0)*c);
  }

  //optional bits
  void writePerformanceData(string prefix);

  // dump / load may be overloaded
  //  void dump(ostream &os);
  //  void load(istream &is);

  //the boring junk
  OpenHash(BitSequence *pat);
  ~OpenHash();

  //muchos boring
  inline static Location val2loc(const val_type& a){return Location(a.first,a.second);}
  inline static val_type loc2val(const Location& a){return val_type(a.seqno,a.pos);}
  static bool lessthan(const val_type& a,const val_type& b);

 protected:
  Ecolist *fasta;
  HashKey findAddr(const HashKey start,const HashVal &val);
  HashKey keysFree,keysFreeWarning;

#ifdef HASHPROFILE
  map<size_t,size_t> perfProbeHisto;
  size_t perfProbeCount,perfFindAddrCount;
#endif

 public:
  static void init(SeqPos _seq_count,word _longestSeq,word _totalHashLength,SeqPos _hash_bits);
  
};

#endif
