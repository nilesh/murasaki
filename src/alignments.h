/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/
//Kris Popendorf (2007)
//post-processing alignment management stuff

#ifndef __ALIGNMENTS_H_
#define __ALIGNMENTS_H_

#include <vector>
#include <map>
#include <list>
#include <iostream>
#include <set>
#include <algorithm>
#include <fstream>
#include "itree.h"
#include "dinkymath.h"

#if defined(__FreeBSD__)
typedef u_long ulong; //silly bsd'rs
#endif
#if defined(__APPLE__)
typedef unsigned long ulong;
typedef unsigned int uint;
#endif

using namespace std; // >_<;;; call me lazy...

class Anchor;
class Alignment;
class RegionSet;
typedef Itree::Interval<long> region;
typedef Itree::itree<long,Anchor*> usedtree;

extern uint seqCount;
extern vector<string> seqs;
extern bool seqinit;

inline bool isZero(const region& r){return r==region(0,0);}
inline bool isZero(const usedtree::iterator& r){return r.key()==region(0,0);}
inline bool isRev(const region& r){return r.start<0;}
inline region invert(const region& r){return region(-r.stop,-r.start);}
inline region& invertMe(region& r){long t=r.stop; r.stop=-r.start; r.start=-t; return r;}
inline region abs(const region& r){if(isRev(r))return invert(r);else return r;}
inline long length(const region& r){return r.stop-r.start+1;}
bool regionsOverlap(const region &a,const region &b);

bool canonicalize(vector<region> &vr);

ostream& operator<<(ostream& os,Anchor& a);
ostream& operator<<(ostream& os,const RegionSet& a);
ostream& operator<<(ostream& os,Alignment& a);


class RegionSet {
 public:
  vector<region> parts;
  RegionSet(const Anchor&);
  double avgLength() const;
  long riftCount() const;
  inline long islandCount() const {return parts.size()-riftCount();}
  bool isGapped() const;
  RegionSet getIslands() const;
  RegionSet setAbs() const;
 protected:
  RegionSet();
};

class Anchor {
  static int nextId;
public:
  
  vector<usedtree::iterator> parts;
  list<Anchor>::iterator backref;
  int id;
  inline Anchor() : parts(seqCount),id(nextId++) {
    assert(parts.size());
  }

  bool operator==(const Anchor& a);
  bool overlaps(const vector<region> &regions) const;
};

inline region grow(const region& r,long amount){
  if(r.start>0)
    return region(max<long>(1,r.start-amount),r.stop+amount);
  else
    return region(r.start-amount,min<long>(-1,r.stop+amount));
}

inline region growCopy(const region& a,ulong amount){
  region r(a);
  if(r.start>0)
    return region(max<long>(1,r.start-amount),r.stop+amount);
  else
    return region(r.start-amount,min<long>(-1,r.stop+amount));
  return r;
}

inline void coalesce(region &a,const region &b){
  if(isZero(a) || isZero(b))
    return;
  assert((a.start<0) == (b.start<0));
  a.start=min<long>(a.start,b.start);
  a.stop=max<long>(a.stop,b.stop);
}

class Alignment {
public:
  list<Anchor> anchors;
  vector<usedtree> trees;
  int id;
  static int nextId;

  inline Alignment(): trees(seqCount),id(nextId++) {
    assert(trees.size());
  }
  
  inline bool fetchAnchorIds(int seq,const region &a,set<int> &out){
    bool res=false;
    for(usedtree::range_iterator i(trees[seq].in_range(a));i!=trees[seq].end();++i){
      Anchor* anc=*i;
      out.insert(anc->id);
      res=true;
    }
    return res;
  }

  inline bool fetchAnchorIds(int seq,const region &a,set<int> &out,ulong growAnchors){
    bool res=false;
    for(usedtree::range_iterator i(trees[seq].in_range(grow(a,growAnchors)));i!=trees[seq].end();++i){
      Anchor* anc=*i;
      out.insert(anc->id);
      res=true;
    }
    return res;
  }

  inline bool contains(const Anchor& a){
    set<int> matches,temp,context;

    if(!fetchAnchorIds(0,a.parts[0].key(),context))
      return false;

    for(uint i=1;i<seqCount;i++){
      if(!fetchAnchorIds(i,a.parts[i].key(),temp))
	return false;
      set_intersection(temp.begin(),temp.end(),
		       context.begin(),context.end(),
		       inserter(matches,matches.begin()));
      if(matches.empty())
	return false;
      context.clear();
      context.insert(matches.begin(),matches.end());
      matches.clear();
    }
    //context now contains only ids which overlap in all the sets
    return !context.empty();
  }

  inline bool contains(const Anchor& a,ulong extend){
    set<int> matches,temp,context;
    
    if(!fetchAnchorIds(0,growCopy(a.parts[0].key(),extend),context))
      return false;

    for(uint i=1;i<seqCount;i++){
      if(!fetchAnchorIds(i,growCopy(a.parts[i].key(),extend),temp))
	return false;
      set_intersection(temp.begin(),temp.end(),
		       context.begin(),context.end(),
		       inserter(matches,matches.begin()));
      if(matches.empty())
	return false;
      context.clear();
      context.insert(matches.begin(),matches.end());
      matches.clear();
    }
    //context now contains only ids which overlap in all the sets
    return !context.empty();
  }

  inline Anchor& add(const Anchor& refa){
    anchors.push_back(refa);
    Anchor& a=anchors.back();
    a.backref=anchors.end();
    --a.backref;
    assert(a==*(a.backref));
    for(uint seqi=0;seqi<refa.parts.size();seqi++){
      a.parts[seqi]=trees[seqi].insert(a.parts[seqi].key(),&a);
    }
    return a;
  }

  inline Anchor& add(vector<region> &parts){
    anchors.push_back(Anchor());
    Anchor& a=anchors.back();
    a.backref=anchors.end();
    --a.backref;
    assert(a==*(a.backref));
    for(uint seqi=0;seqi<parts.size();seqi++){
      a.parts[seqi]=trees[seqi].insert(parts[seqi],&a);
    }
    return a;
  }

  void mergeAdd(const vector<region> &parts);
  void mergeAdd(const Anchor&);
  void remove(Anchor*);

  inline bool fetchAnchors(int seq,const region &a,set<Anchor*> &out){
    bool res=false;
    for(usedtree::range_iterator i(trees[seq].in_range(a));i!=trees[seq].end();++i){
      Anchor* anc=*i;
      out.insert(anc);
      res=true;
    }
    return res;
  }

  bool findOverlaps(const Anchor& a,set<Anchor*> &context);
  bool findOverlaps(const vector<region> parts,set<Anchor*> &context);
  void boolean_or(Alignment& a,Alignment& b);

};

class SequenceCover {
 public:
  typedef Itree::itree<long,long> TreeType;
  SequenceCover();
  TreeType::iterator merge(region r);
  long totalLength();
  inline TreeType::iterator begin(){return cover.begin();}
  inline TreeType::iterator end(){return cover.end();}
  inline void erase(TreeType::iterator &i){cover.erase(i);}
  TreeType cover;
};

class AnchorFileReader {
 public:
  string filename;
  ulong growAnchors;
 protected:
  int linenum;
  ifstream inf;

 public:
  AnchorFileReader(string _filename,ulong _growAnchors);
  bool readline(vector<region> &out);
};

//funk decls!

string getAnchorPrefix(string str);
void checkSeqs(const char*);
void writeSeqs(const char*);
void loadAnchors(const char* filename,Alignment& tree,ulong growAnchors=0);

#endif

