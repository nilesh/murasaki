/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////
// murasaki project
// seqread.h
// c++ support for reading various sequence formats
//////////////

#ifndef __BINSEQ_H_
#define __BINSEQ_H_

#include "globaltypes.h"
#include "seqread.h"
#include "dinkymath.h"

#include <fstream>
#include <iostream>
#include <list>
#include <vector>
#include <boost/regex.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>

#define BINSEQ_MAGICNUMBER "\xCC\x33\xFF" //this is purple in rgb hex
#define CHARCAST(x) ((const char*)&(x))
#define VOIDOFFSET(x,off) ((const char*)(x)+static_cast<ptrdiff_t>(off))
#define VOIDGET(T,x,off) (*(T*)(VOIDOFFSET(x,off)))

using namespace std;
namespace io = boost::iostreams;

class SequenceBinaryMetadata {
public:
  unsigned char formatVersion;
  string suffix;
  SequenceBinaryMetadata();
};

class SeqBinException {
public: 
  SeqBinException(const string& reason):
    reason_(reason) {}
  string reason() const { return reason_; }
  const char* what() const { return reason_.c_str();}

 private:
    string reason_;
};

class SequenceBinary {
public:
  typedef pair<size_t,size_t> Region;
protected:
  void init();
  SequenceBinaryMetadata formatMetaData;
  word _length;
  word bit_count,word_count;
  word *_words;
  word* allocWords();
  int seqdataMalloc;

  int fd;
  void *fdmem;
  size_t fdmem_len;
  string baseFilename,binaryFilename;

  //debugging bits
  char rawBase(size_t n);
  word rawBaseBits(size_t n);
  char bitToBase(word w);
public:
  word counters[8]; //unmasked[4],masked[4]
  word regionSizes[3];
  vector<const char*> subSeqNamesP;
  vector<string> subSeqNames;
  vector<Region> subSeqs,readableRegions,unmaskedRegions;
  Region *regions[3];
  void fillCopyVectors();
  inline string getBinaryFilename(){return binaryFilename;}
  inline string getBaseFilename(){return baseFilename;}
  inline const word* words()const {return _words;}
  inline const word getWordCount()const {return word_count;}
  inline size_t length()const {return _length;}
  
  inline string deriveLocalBinaryFilename(){return deriveLocalBinaryFilename(baseFilename);}
  string deriveLocalBinaryFilename(string filename);
  static string deriveBinaryFilename(string filename);
  static bool exists(string filename); //test if a binary file exists for filename
  bool save(string filename);
  bool save();
  ostream& asFasta(ostream &os,int nmaskR=1);
  SequenceBinary(string filename,bool complete=false);
  SequenceBinary(SequenceFileReader &reader);
  ~SequenceBinary();
};

class DeSerial {
public:
  void *m;
  size_t offset;

  DeSerial(void *_m,size_t _offset=0);

  template<typename T>
  void map(T* &dst,int count){
    dst=(T*)VOIDOFFSET(m,offset);
    int size=sizeof(T);
    offset+=size*count;
  }

  template<typename T>
  void get(T* dst,int count){
    int size=sizeof(T);
    for(;count>0;offset+=size,count--,dst++){
      *dst=VOIDGET(T,m,offset);
    }
  }

  template<typename T>
  T& get(T &dst){
    get(&dst,1);
    return dst;
  }
};

#endif
