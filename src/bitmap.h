#ifndef _BITMAP_H_
#define _BITMAP_H_

#include "globaltypes.h"

class bitmap {
 public:
  inline bitmap(word c):
  map(new word[(c-1)/WORDSIZE+1]),
    size((c-1)/WORDSIZE+1)
      {
	memset(map,0,sizeof(word)*size);
      }
  
  inline void clear(){
    memset(map,0,sizeof(word)*size);
  }
  
  inline bool checkAndSet(word hash){
    word idx=hash/WORDSIZE;
    word mask=((word)1U)<<(MODWORDSIZE(hash));
    bool res=(map[idx] & mask);
    if(!res){ //it seems that leaving this check in is actually faster!
      map[idx]|=mask;
    }
    return res;
  }
  inline bool check(word hash) const{
    word idx=hash/WORDSIZE;
    word mask=((word)1U)<<(MODWORDSIZE(hash));
    bool res=(map[idx] & mask);
    return res;
  }
  inline void set(word hash){
    word idx=hash/WORDSIZE;
    word mask=((word)1U)<<(MODWORDSIZE(hash));
    if(map[idx] & mask)
      map[idx]|=mask;
  }
  inline void flip(word hash){
    word idx=hash/WORDSIZE;
    word mask=((word)1U)<<(MODWORDSIZE(hash));
    map[idx]^=mask;
  }
  inline void unset(word hash){
    word idx=hash/WORDSIZE;
    word mask=((word)1U)<<(MODWORDSIZE(hash));
    if(map[idx] & mask)
      map[idx]&=~mask;
  }
  inline ~bitmap(){
    delete[] map;
  }
 protected:
  word *map;
  word size;
};

#endif
