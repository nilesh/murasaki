/*
Copyright (C) 2006-2008 Keio University
(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)

This file is part of Murasaki.

Murasaki is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Murasaki is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.
*/

////////
// arrayhash defs
// Kris Popendorf
///////

#ifndef ARRAYHASH_H_
#define ARRAYHASH_H_

#include "sequence.h"
#include "cmultiset.h"

class ArrayHash : public Hash {
 public:
  //my own lovely bunch of coconuts
  typedef cmultiset<HashVal>*  MHash; //multihash
  typedef cmultiset<HashVal>::iterator HashIte;

  void clear();
  void add(const HashKey key,const HashVal &val);
  void getMatchingSets(HashKey key,list<LocList> &sets);
  void lookup(HashKey key,LocList &locList);
  bool emptyAt(const HashKey key);
  word sizeAt(const HashKey key);
  word sizeAt(const HashKey key,const HashVal &val);

  ArrayHash(BitSequence *pat);

  void dump(ostream &os);
  void load(istream &is);

  static const word linear_cost(word c) {
    return sizeof(Location)*c + sizeof(Location)*c/4; //the +25% term is an estimate of memory lost to external fragmentation
  }
  static const word bucket_prep_cost(word c) {
    return (sizeof(void*)+sizeof(cmultiset<HashVal>))*c;
  }

  bool rawSanityCheck();
  bool rawSanityCheck(word base);

 protected:
  void pickup(LocList &locList, pair<HashIte,HashIte>);
  MHash *fasta;
  pair<HashIte,HashIte> activeRange;
};

#endif
