#!/usr/bin/perl -w

#Copyright (C) 2006-2008 Keio University
#(Kris Popendorf) <comp@bio.keio.ac.jp> (2006)
#
#This file is part of Murasaki.
#
#Murasaki is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Murasaki is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Murasaki.  If not, see <http://www.gnu.org/licenses/>.

##################
## convert mauve alignments to Murasaki anchor files -- krisp
##################

use File::Basename;
use Getopt::Long;
use Pod::Usage;
#use Data::Dump qw{dump};

use strict;
my ($help,$man,$align_type);
my $autoout=1;
our $flexible=0;
our $signed=1;
my $useAlignment;
GetOptions('help|?' => \$help, man => \$man, 'autoout!'=>\$autoout);
pod2usage(1) if $help or $#ARGV<0;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

my ($filename,$outfile)=@ARGV;

my ($basename,$path,$suffix) = fileparse($filename);
die "Input file not found: $filename" unless -e $filename;

$outfile="$path/$basename.aligned.anchors" if $autoout;
print "Outfile is $outfile\n" if $autoout;

if($outfile){
  open(OUTF,">$outfile");
} else {
  open(OUTF,">-");
}

my %mauvedat=%{loadMauveAlignment($filename)};
foreach my $lcb (@{$mauvedat{LCBs}}){
  print OUTF (join("\t",map {($_->{start},$_->{stop},($_->{start}<0 ? "-":"+"))} (@$lcb))."\n");
}

if($basename){ #we can also make the seqs file
  open(my $seqfh,">$basename.aligned.seqs");
  local $,="\n";
  local $\="\n";
  print $seqfh (map {$_->{seqFile}} @{$mauvedat{seqs}});
}

sub loadMauveAlignment {
  my $alignment=shift;
  open(MAUVE,"<$alignment");
  <MAUVE>=~m/FormatVersion\s+(\d+)/ or die "Not a mauve file: $alignment";
  my $version=$1;
  my @seqs=();
  do {print "This program is written for Mauve Format Version 4.\n
This file is version $version. Weird stuff may happen.\n"; $flexible=1;} if $version!=4;
  <MAUVE>=~m/SequenceCount\s+(\d+)/ or die "Unknown sequence count\n";
  my $seqCount=$1;
  while(<MAUVE>){
    next unless m/Sequence(\d+)File\s+(\S.*)/;
    my ($seqId,$seqFile)=($1,$2);
    $_=<MAUVE>;
    m/Sequence${seqId}Length\s+(\d+)/ or $flexible or die "Input file is weird: $_";
    my $seqLength=$1;
    $seqs[$seqId]={'seqId' => $seqId,'seqFile' => $seqFile,'seqLength'=>$seqLength, 'seqName' => getName($seqFile) };
    last if $seqId==$seqCount-1;
  }

  my @LCBs=();
  $_=<MAUVE>;
  m/IntervalCount\s(\d+)/ or $flexible or die "Interval Count line weird: $_";
  my $LCBCount=$1;
  while(<MAUVE>){
    chomp;
    if(m/^Interval\s(\d+)/){
      my $LCBId=$1;
      while($_ ne '') {
	$_=<MAUVE>;
	chomp;
	last if $_ eq '';
	if(m/^GappedAlignment/){
	  $_=<MAUVE>;
	  my ($length,@start)=split(/\s+/);
	  my @alignments=map {local $_=<MAUVE>;chomp;$_} 0..$#start;
	  my @subi=map {0} @start;
	  my @substart=map {0} @subi;
	  my $in;
	  my $inre=qr/[^\-]/;
	  local $|=1;
	  while(max(@subi)<$length){
	    my @c=map {substr($alignments[$_],$subi[$_],1)} 0..$#subi;
	    if(!defined $in){
	      $in=(grep($inre,@c)==scalar(@c));
	    }
	    if(grep($inre,@c)==scalar(@c)){ #all in?
	      if($in){
		#nothing to do really...
	      }else{
		@substart=@subi; #starting new region
		$in=1;
	      }
	      foreach my $si (@subi){$si++} #advance them all!
	    }else{ #someone's out
	      if($in){ #region ended, add anchor
		my @lstart=map {$start[$_]+$substart[$_]} 0..$#start;
		my @stop=map {$start[$_]+$subi[$_]} 0..$#start;
		my @LCB=map {
		  { start => $lstart[$_], stop => $stop[$_], LCBId => $LCBId }} 0..$#stop;
		$in=0;
		push(@LCBs,\@LCB);
		foreach my $si (@subi){$si++} #advance them all!
	      }else{ #in a gap advance iterators currently pointed at gaps
		foreach my $i (0..$#subi){
		  $subi[$i]++ if !(substr($alignments[$i],$subi[$i],1)=~m/$inre/);
		}
	      }
	    }
	  }
	}else{ #simple mum region (perfect match)
	  my ($length,@start)=split(/\s+/);
	  next if (grep {$_==0} @start)>0;
	  my @stop=map {$_+$length} @start;
	  my @LCB=map {
	    { start => $start[$_], stop => $stop[$_], LCBId => $LCBId }} 0..$#stop;
	  push(@LCBs,\@LCB);
	}
      }
    }
  }
  return {'seqs' => \@seqs, 'LCBs' => \@LCBs};
}

sub max {
  my ($r,@l)=@_;
  foreach my $v (@l){
    $r=$v if $v>$r;
  }
  return $r;
}

sub getName {
  my @ret=map {
    my ($name,$path,$suffix) = fileparse($_, qr{\.[^.]*});
    $name
    } @_;
  return @ret if $#_;
  return $ret[0];
}

__END__

=head1 NAME

mauveAlignment2anchors - converts a Mauve alignment into Murasaki
anchor format based on gapped alignment data (each gap triggers a new
anchor). Note this refers to the Mauve alignment format, not the LAGAN
.alignment file that Mauve also outputs.


=head1 SYNOPSIS

mauveAlignment2anchors <input> [output]

=head1 OPTIONS

  --autoout -- create output file name automatically (on by default)
  --noautoout -- disable the above "autoout" option
